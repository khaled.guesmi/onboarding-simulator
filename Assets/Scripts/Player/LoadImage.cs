﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI ;
using UnityEngine.Networking;

namespace onboard
{
    public class LoadImage : MonoBehaviour
    {
        public RawImage rimg;
        public string urlImage;



        public void SetUrlImage(string url)
        {
            urlImage = url;
        }


        void Start()
        {

            LoadImageFromUrl(urlImage, rimg);

        }
        void FixedUpdate()
        {


            LoadImageFromUrl(urlImage, rimg);

        }


        public void LoadImageFromUrl(string url, RawImage raw)
        {

            StartCoroutine("loadImageInUnity", url);

        }

        public IEnumerator loadImageInUnity(string url)
        {

            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
#if UNITY_EDITOR
                Debug.Log(www.error);
#endif

            }
            else
            {
                Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                if (myTexture != null)
                {
                    rimg.texture = myTexture;
                }


            }

        }


    }
}

