﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



namespace onboard
{
    public class PlayerIDCard : MonoBehaviour
    {
        #region Properties
        [Header("Text Areas UI ")]
        [Space]
        public TextMeshProUGUI nameText;
        public TextMeshProUGUI postText;
        public TextMeshProUGUI joinedText;
         
        public TextMeshProUGUI tagsTxt;
        [Header("Input Field Areas UI ")]
        [Space]
        public TMP_InputField tel;
        public TMP_InputField mail;
        public TMP_InputField hobbies;
        

        [Header("Tags Input Field ")]
        [Space]
        public GameObject prefab1;
        public GameObject prefab2;
        public GameObject tagHolder;
        public string HobbiesTag = "";
        private Queue<string> tags;
        bool isDisSelected = false;
        [Header("Quote Input Field ")]
        [Space]
        public TMP_InputField citation;
        public GameObject prefab3;
        public GameObject quoteHolder;
        [Tooltip("this is to make the player wait until the data is saved in database")]
        public GameObject WaitPannel;

        public static PlayerIDCard _instance;
        #endregion

        #region default Methodes
        void Start()
        {

            tags = new Queue<string>();

        }
        void Update()
        {

            updateInputs(); // always try to update the inputs 
            setCard(StaticStrings.currentPlayer);


        }

        void OnGUI()
        {

            if (isDisSelected)
            {
                CreateNewTag();
                isDisSelected = false;
            }




        }
        #endregion
        #region Singletons

        public static PlayerIDCard Instance
        {

            get
            {

                if (_instance == null)
                {
                    _instance = FindObjectOfType<PlayerIDCard>();
                    
                }
                return _instance;
            }
        }
        #endregion
        #region  custon Methodes 


        public void setCard(Player p)
        {
            if (p != null)
            {
                nameText.text = p.name + "";
                nameText.text += p.family_name;
                postText.text = p.poste;
                System.DateTime theTime = DateTime.Now;
                string date = theTime.Year + "-" + theTime.Month + "-" + theTime.Day;
                joinedText.text = date;
                

            }
            else
            {
                nameText.text = "default ";
                nameText.text += "name";
                postText.text = "none";

                System.DateTime theTime = DateTime.Now;
                string date = theTime.Year + "-" + theTime.Month + "-" + theTime.Day;
                joinedText.text = date;
                 
            }


        }


        public void ChangeTextInput(TMP_InputField inputField)
        {

            if (Input.GetKey(KeyCode.Space)) //press space to create next tag 
            {


                String s = inputField.text;
                // Initiate tag prefabs 
                //addition tag diff back 
                #region tag instantiate
                if (s != "" )
                {
                    int range = UnityEngine.Random.Range(0, 10);
                    if (range % 2 == 0)
                    {
                        GameObject p = Instantiate(prefab1, transform.position, Quaternion.identity);
                        p.transform.SetParent(tagHolder.transform);
                        TextMeshProUGUI t = p.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                        t.text = s;
                    }
                    else
                    {
                        GameObject p = Instantiate(prefab2, transform.position, Quaternion.identity);
                        p.transform.SetParent(tagHolder.transform);
                        TextMeshProUGUI t = p.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                        t.text = s;
                    }
                }
                
               

                #endregion

                tags.Enqueue(s);
                tagsTxt.text += s + " ";
        

                inputField.text = "";

            }
        }

        public void GenerateQuote()
        {
            string s = citation.text;
            GameObject p = Instantiate(prefab3, transform.position, Quaternion.identity);
            TextMeshProUGUI t = p.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            p.transform.SetParent(quoteHolder.transform);
           
            t.text = s;
        }
        public void CreateNewTag()
        {

            int i = 0;
            foreach (string x in tags)
            {
                if (x != "")
                {
                    HobbiesTag += x;

                    int width = x.Length * 8;
                    GUI.TextField(new Rect(250 + i, 300, width, 20), x, 25);
                    i += width;
                }

            }


        }

        void CreateTag(String x, int i)
        {
            HobbiesTag += x;

            int width = x.Length * 8;
            GUI.TextField(new Rect(250 + i, 300, width, 20), x, 25);
            i += width;
        }

        public void printPlayerInformation()
        {
             WaitPannel.SetActive(true);
            StartCoroutine(RestClient.Instance.Set());
            WaitPannel.SetActive(false);
        }


        void updateInputs()
        {
            StaticStrings.currentPlayer.email = mail.text;
            StaticStrings.currentPlayer.tel = tel.text;
            
            StaticStrings.currentPlayer.birthday = joinedText.text;
            if (HobbiesTag != "")
            {
                StaticStrings.currentPlayer.hobbies = HobbiesTag;
            }

            StaticStrings.currentPlayer.citation = citation.text;
        }

        public void CreatetagsNow()
        {
            isDisSelected = true;
        }


        #endregion


    }

}