﻿using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace onboard
{
    public class Movement : MonoBehaviour
    {

        #region properties
        [Header("player movement attributes  ")]
        public float playerSpeed;
        public float sprintSpeed = 4f;
        public float walkSpeed = 2f;
        public float mouseSensitivity = 2f;
        public float rotationSpeed = 2f;
        public GameObject map_expaded;
        public GameObject mini_Map;
        public GameObject questUI;


        public Animator anim;
        private Rigidbody rigidBody;

        bool isTurning = false ;
        bool isWalking = false;
        bool isActive  = true;
        bool isExpanded= false;
        bool goRight = false;
        bool goLeft = false;
        #endregion

        #region methodes

        void Awake()
        {
            isTurning = false;
            isWalking = false;
            rigidBody = GetComponentInChildren<Rigidbody>();
            
        }
        // Use this for initialization
        void Start()
        {
            if (mini_Map != null && map_expaded != null && questUI != null)
            {
                isActive = true;
                isExpanded = false;

            }
            playerSpeed = walkSpeed; 
        }

        // Update is called once per frame
        void Update()
        {
            
            if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)
            {
                 
                if (Input.GetAxisRaw("Horizontal") > 0.5f)
                {
                    goRight = true;
                    anim.SetBool("goRight", goRight);
                   

                    transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime);
            
                }
                if (Input.GetAxisRaw("Horizontal") < -0.5f)
                {
                    goLeft = true;
                    anim.SetBool("goLeft", goLeft);
                    transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime);
                   
                }
            }
            if (Input.GetAxisRaw("Horizontal") == 0)
            {
                if (goRight)
                {
                    goRight = false;
                    anim.SetBool("goRight", goRight);
                }
                if (goLeft)
                {
                    goLeft = false;
                    anim.SetBool("goLeft", goLeft);
                }
                 
            }
            
            if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f)
            {
                isWalking = !isWalking;
                if (isWalking)
                {
                    anim.SetBool("isWalking" , true);

                    transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * playerSpeed * Time.deltaTime);
                }
                else
                {
                    anim.SetBool("isWalking", false);
                }
               
                

            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                transform.Rotate(rotationSpeed * Vector3.right);
                goRight = !goRight;
                goLeft = false;
                anim.SetBool("isTurningR", goRight);
                anim.SetBool("isTurningL" , goLeft);
               
                StartCoroutine(WaitForFewSeconds(2f));
                   
             
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                goLeft = !goLeft;
                goRight = false;
                anim.SetBool("isTurningR", goRight);
                anim.SetBool("isTurningL", goLeft);
               StartCoroutine(WaitForFewSeconds(1f));
               
            }
            
            
            if (Input.GetKeyDown(KeyCode.M))
            {
                if (mini_Map.activeInHierarchy)
                {
                    mini_Map.SetActive(false);
                    map_expaded.SetActive(true);
                }
                else
                {
                    mini_Map.SetActive(true);
                    map_expaded.SetActive(false);
                }
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                if (questUI.activeInHierarchy)
                {
                    questUI.SetActive(false);
                }
                else
                {
                    questUI.SetActive(true);
                }
            }

        }

        IEnumerator WaitForFewSeconds(float time)
        {
            yield return new WaitForSeconds(time);
            goRight = false;
            goLeft = false;
            anim.SetBool("isTurningL", goLeft);
            anim.SetBool("isTurningR", goRight);
        }

        #endregion
    }

}