using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace onboard
{
    [System.Serializable]
    public class Player
    {
        #region Properies
        public int id;
        public int badge_id;
        public string name;
        public string family_name;
        public string poste;
        public string tel;
        public string email;
        public string birthday;
        public string l_facebook;
        public string l_twitter;
        public string l_linkedin;
        public string hobbies;
        public string citation;
        public string accepted_turms;
        private static Player _inst;
        #endregion
        public static Player Instance
        {
            get
            {
                if (_inst is null)
                {
                    _inst = new Player();
                    _inst.Init();
                }
                return _inst;
            }

        }

        private void Init()
        {

        }

        public override string ToString()
        {
            return "Player data saved : Name   " + name +"\n family name    "+family_name+ "\n email   "+email;

        }


    }


}