﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
 
using UnityEngine.UI;
using TMPro;

namespace onboard
{
    public class TermsOfService : MonoBehaviour
    {
        #region properties
        public Toggle terms;
        private static  TermsOfService _instance;
        private string WEB_URL_Post = "http://localhost/OnboardWS/SetDocStat.php?id=";
        private string URL_Get_ById = "http://localhost/OnboardWS/getdoc.php?id=";
        //**********************************
        int status;
        public bool useSubtitle = true;
        public float delay_time = 1f;
        //*******************************
        public GameObject success_txt;
        public GameObject fail_txt;
        public GameObject parent;
        public AudioAction saved;
        public AudioAction notSaved;
        //**************************
        public TMP_Text slot_name;
        public TMP_Text document_title;
        public TMP_Text document_subtitle;
        public TMP_Text document_main;
        Document[] documents;
        Document d = new Document();
        #endregion


        #region Singletons

        public static TermsOfService Instance
        {

            get
            {

                if (_instance == null)
                {
                    _instance = FindObjectOfType<TermsOfService>();
                    if (_instance == null)
                    {
                        GameObject go = new GameObject();//create an empty gameobject 
                        go.name = typeof(TermsOfService).Name;//modify name 
                        go.AddComponent<TermsOfService>();// add this script to the new gameobject 
                        DontDestroyOnLoad(go);//dont destroy the game object on load 
                    }
                }
                return _instance;
            }
        }
        #endregion

        void Start()
        {
            parent.SetActive(false);

            if (!useSubtitle)
            {
                document_subtitle.gameObject.transform.parent.gameObject.SetActive(false);
            }
     
            AssignDocumentData();
        }


        public void ChangedStatus()
        {
            if (terms.isOn)
            {
                status = 1;
                StaticStrings.currentPlayer.accepted_turms = status + "";
           
            #if UNITY_EDITOR
                Debug.Log("stat : " + StaticStrings.currentPlayer.accepted_turms);
            #endif
            }
            else
            {
                status = 0;
                StaticStrings.currentPlayer.accepted_turms = status + "";
 
            #if UNITY_EDITOR
                Debug.Log("stat : " + StaticStrings.currentPlayer.accepted_turms);
            #endif
            }
            PlayerPrefs.SetString("player_accepted_turms", StaticStrings.currentPlayer.accepted_turms);
        }

        //save toggle response
        public IEnumerator SetTerms()
        {
            WEB_URL_Post += "1&accepted_turms=" + StaticStrings.currentPlayer.accepted_turms;
            using (UnityWebRequest www = UnityWebRequest.Get(WEB_URL_Post))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError)
                {
                    StartCoroutine(ShowResult(0));
#if UNITY_EDITOR
                    Debug.Log(www.error.ToString());
#endif
                }
                else
                {

                    if (www.isDone) //test if he connection is reached 
                    {
                        StartCoroutine(ShowResult(1));
                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

#if UNITY_EDITOR
                        Debug.Log(JsonResult);
#endif



                    }
                }
            }
        }

        //couroutine save toggle response
        public void SaveUpdates()
        {
            StartCoroutine(SetTerms());
        }

        IEnumerator ShowResult(int status)
        {
            if (status == 0)
            {
         
                    parent.SetActive(true);
                    fail_txt.SetActive(true);
                   notSaved.Act();
                   yield return new WaitForSeconds(delay_time);
                    fail_txt.SetActive(false);
                      parent.SetActive(false);

               
            }
            else
            {
            
                    parent.SetActive(true);
                    success_txt.SetActive(true);
                    saved.Act();
                    yield return new WaitForSeconds(delay_time);
                    success_txt.SetActive(false);
                    parent.SetActive(false);    
                

            }
           
          

        }

       public void AssignDocumentData()
        {

            Document d1 = GetWantedDocument();
#if UNITY_EDITOR
            Debug.Log("D1:"+d1.ToString());
#endif


            if (d1!=null&&d1.title == "terms&Policy")
            {
                slot_name.text = d1.title;
                document_title.text = d1.title;
                document_subtitle.text = "main document";
                document_main.text = d1.description;
            }
            else
            {

                StartCoroutine(GetDocById(1));
            }
          
            
        }
        public IEnumerator GetDocById(int id)
        {

            URL_Get_ById += id;

            using (UnityWebRequest www = UnityWebRequest.Get(URL_Get_ById))
            {
                yield return www.SendWebRequest();

                if (!www.isNetworkError)
                {

#if UNITY_EDITOR
                    Debug.Log(www.error + "\nNetwork connection didn't fail " + URL_Get_ById);
#endif

                    if (www.isDone)
                    {


                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);



                        if (JsonResult != "")
                        {
                            documents = JsonHelper.getJsonArray<Document>(JsonResult);
                            foreach (var i in documents)
                            {
                                d = i;
                            }
#if UNITY_EDITOR
                           Debug.Log(d.ToString());
#endif
 

                            slot_name.text = d.title;
                            document_title.text = d.title;
                            document_subtitle.text = "main document";
                            document_main.text = d.description;

                        }
                    }
                }

            }
        }
        public Document GetWantedDocument()
        {
            Document local_doc = new Document();
            if (StaticStrings.allDocuments!= null)
            {
                for (int i = 0; i < StaticStrings.allDocuments.docs.Count; i++)
                {
                    if (StaticStrings.allDocuments.docs[i].title.Equals("terms&Policy"))
                    {
                        local_doc.title = PlayerPrefs.GetString("doc_title_" + StaticStrings.allDocuments.docs[i].id);
                        local_doc.description = PlayerPrefs.GetString("doc_desc_" + StaticStrings.allDocuments.docs[i].id);
                        local_doc.image = PlayerPrefs.GetString("doc_image_" + StaticStrings.allDocuments.docs[i].id);

                        local_doc.link = PlayerPrefs.GetString("dec_link_" + StaticStrings.allDocuments.docs[i].id);

                        return local_doc;
                    }

                }
            }
            else
            {
                string title = "terms&Policy";
                int index = 0;
                while (index !=-1)
                {
#if UNITY_EDITOR
                    Debug.Log(index + "this is doc index");
#endif
                    if (PlayerPrefs.GetString("doc_title_" + index).Equals(title))
                    {
                        local_doc.title = PlayerPrefs.GetString("doc_title_" + index);
                        local_doc.description = PlayerPrefs.GetString("doc_desc_" + index);
                        local_doc.image = PlayerPrefs.GetString("doc_image_" + index);

                        local_doc.link = PlayerPrefs.GetString("dec_link_" + index);
                        index = -1;
                        return local_doc;
                    }
                    else
                    {
                        index++;
#if UNITY_EDITOR
                        Debug.Log(PlayerPrefs.GetString("doc_title_" + index) + "this is doc title");
#endif
                    }
                }
               
                    
                 
            }
           
            return local_doc;
        }
    }

}
