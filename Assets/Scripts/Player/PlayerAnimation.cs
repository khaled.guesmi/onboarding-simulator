﻿using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




namespace onboard
{
    public class PlayerAnimation 
    {
        #region properties

        private Animator _animator;
        private int animSpeed = Animator.StringToHash("Speed");
        string animTurn =  "isTurning" ;

        #endregion

        #region initiation
        public void Init(Animator anim)
        {
            _animator = anim;
        }
        #endregion

        #region methodes
        public void UpdateAnimation(float speed)
        {
        
            _animator.SetFloat(animSpeed , speed );
        }

public void UpdateAnimation2(float speed)
        {
            UnityEngine.Debug.Log(speed + "this is anim speed");
        
            _animator.SetFloat(animSpeed , speed );
        }
        public void TurnPlayer(bool isTurning)
        {
            _animator.SetBool(animTurn, isTurning);
        }
        #endregion
    }

}
