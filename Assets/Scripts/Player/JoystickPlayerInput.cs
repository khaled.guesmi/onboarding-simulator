﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace onboard
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(LineRenderer))]
    public class JoystickPlayerInput : MonoBehaviour
    {
        #region properties
        [Header("Mouse Click Movement")]
        NavMeshAgent agent;
        LineRenderer myLineRendered;
        Quaternion targetRotation;
        bool turning;
        public GameObject questUI;
        private PlayerAnimation playerAnimation = new PlayerAnimation();
        private Interraction prevInteractable;
        bool isActive = true;
        public GameObject map_expaded;
        public GameObject mini_Map;
        //map test
        public bool isExpanded = false;

        //test marker
        [SerializeField]
        GameObject markerPrefab;
       [SerializeField]
        Transform parentVisual;

        [Header("Mouse keyboard Movement")]
        public float playerSpeed;
        public float sprintSpeed = 4f;
        public float walkSpeed = 2f;
        public float mouseSensitivity = 2f;
        public float rotationSpeed = 2f;
        Animator anim;
        private Rigidbody rigidBody;

        bool isTurning = false;
        bool isWalking = false;
       
        bool goRight = false;
        bool goLeft = false;
        #endregion

        #region default methodes
        void Awake()
        {
            //protect it and cash it 
            agent = GetComponent<NavMeshAgent>();
            myLineRendered = GetComponent<LineRenderer>();
            anim = GetComponentInChildren<Animator>();
        }
        void Start()
        {
            playerAnimation.Init(GetComponentInChildren<Animator>());
            isActive = true;
            isExpanded = false;
            isTurning = false;
            isWalking = false;
            rigidBody = GetComponentInChildren<Rigidbody>();
            myLineRendered.startWidth = 0.15f;
            myLineRendered.endWidth   = 0.15f;
            myLineRendered.positionCount = 0;
            //search for the animator in children
            playerSpeed = walkSpeed;

        }
     
        // Update is called once per frame
        void Update()
        {
            #region MiniMap & UI Quest

            if (Input.GetKeyDown(KeyCode.M))
            {  
             if (mini_Map.activeInHierarchy)
                {
                    mini_Map.SetActive(false);
                    map_expaded.SetActive(true);
        
                }
                else
                {
                    mini_Map.SetActive(true);
                    map_expaded.SetActive(false);
           
                }
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                
           if (questUI.activeInHierarchy)
                {
                    questUI.SetActive(false);
                 
                }
                else
                {
                    questUI.SetActive(true);
                 
                } 
            }

            #endregion


            #region keyboard movement
            if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)
            {

                if (Input.GetAxisRaw("Horizontal") > 0.5f)
                {
                    goRight = true;
                    anim.SetBool("goRight", goRight);


                    transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime);

                }
                if (Input.GetAxisRaw("Horizontal") < -0.5f)
                {
                    goLeft = true;
                    anim.SetBool("goLeft", goLeft);
                    transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * playerSpeed * Time.deltaTime);

                }
            }
            if (Input.GetAxisRaw("Horizontal") == 0)
            {
                if (goRight)
                {
                    goRight = false;
                    anim.SetBool("goRight", goRight);
                }
                if (goLeft)
                {
                    goLeft = false;
                    anim.SetBool("goLeft", goLeft);
                }

            }

            if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f)
            {
                isWalking = !isWalking;
                if (isWalking)
                {
                    anim.SetBool("isWalking", true);

                    transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * playerSpeed * Time.deltaTime);
                    StartCoroutine(WaitForFewSeconds(1f));
                }
                else
                {
                    anim.SetBool("isWalking", false);
                }



            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                transform.Rotate(rotationSpeed * Vector3.right);
                goRight = !goRight;
                goLeft = false;
                anim.SetBool("isTurningR", goRight);
                anim.SetBool("isTurningL", goLeft);

                StartCoroutine(WaitForFewSeconds(1f));


            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                goLeft = !goLeft;
                goRight = false;
                anim.SetBool("isTurningR", goRight);
                anim.SetBool("isTurningL", goLeft);
                StartCoroutine(WaitForFewSeconds(1f));

            }

            #endregion


            #region mouse btn  movement
            if (Input.GetMouseButtonDown(0) && !Extensions.IsMouseOverUI())
            {
                OnClick();
            }

            if (turning && transform.rotation == targetRotation)
            {
                turning = false;
            }
            if (turning && transform.rotation != targetRotation)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 15f * Time.deltaTime);
            }
            if (!agent.isStopped && CheckIfArrived())
            {
                markerPrefab.transform.SetParent(transform);

                markerPrefab.SetActive(false);

                agent.isStopped = true;
            }
            else if (agent.hasPath)
            {
                DrawPath();
            }



            playerAnimation.UpdateAnimation(agent.velocity.sqrMagnitude);//update our animation 
            #endregion


        }
        public bool CheckIfArrived()
        {
            return (!agent.pathPending && agent.remainingDistance <= agent.stoppingDistance);
        }
        #endregion
        IEnumerator WaitForFewSeconds(float time)
        {
            yield return new WaitForSeconds(time);
            goRight = false;
            goLeft = false;
            anim.SetBool("isTurningL", goLeft);
            anim.SetBool("isTurningR", goRight);
            anim.SetBool("isWalking", false);
        }

        #region custom methodes


        public void MovePlayer(Vector3 targetPosition)
        {
            turning = false;
            agent.isStopped = false;

            markerPrefab.SetActive(true);
            markerPrefab.transform.SetParent(parentVisual);
            markerPrefab.transform.position = targetPosition;
            agent.SetDestination(targetPosition);
            DialogueSystem.Instance.HideDialog();
        }


        public void FaceTarget(Vector3 targetPosition)
        {
            turning = true;
            Vector3 vectorDirection = targetPosition - transform.position;
            vectorDirection.y = 0f;
            targetRotation = Quaternion.LookRotation(vectorDirection);
        }

        public bool CheckDestinationReach()
        {
            return (!agent.pathPending && agent.remainingDistance <= agent.stoppingDistance);
        }


        void OnClick()
        {
            RaycastHit  hit;
            Ray camToscreen = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(camToscreen , out hit ,Mathf.Infinity))
            {
                if (hit.collider != null)
                {
#if UNITY_EDITOR
                   Debug.Log("hit :" + hit.collider.name);
#endif
 
                    //interract with clicked objects 
                    Interraction interraction = hit.collider.GetComponent<Interraction>();

                    if (interraction != null )
                    { 
                        if (prevInteractable != null)
                            prevInteractable.StopAllCoroutines();

                        prevInteractable = interraction;
                     
                        for (int i = 0; i < interraction.toggleObject.Length; i++)
                        {
                            interraction.toggleObject[i].SelectedObjective();
                        } 

                        MovePlayer(interraction.InterractPosition()); //move to selected position
                        interraction.Interract(this);//couroutine test for arriving 
                    }
                    else
                    {
                 
                        MovePlayer(hit.point);
                    }

                }
            }

        }


        #endregion

        #region Path creation
        void DrawPath()
        {
            myLineRendered.positionCount = agent.path.corners.Length;
            myLineRendered.SetPosition(0, transform.position);
            if (agent.path.corners.Length <2)
            {
                return;
            }
            for (int i = 0; i < agent.path.corners.Length; i++)
            {
                Vector3 pointPosition = new Vector3(agent.path.corners[i].x , agent.path.corners[i].y , agent.path.corners[i].z);
                myLineRendered.SetPosition(i, pointPosition);
            }
        }
        #endregion
    }
}