﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace onboard
{
    public class PlayerQuest : Actions
    {
        public int amount = 1;
        public int quest_id;
        public bool done = false;
        int objectif_amount;
        int current_amount;
        [Tooltip("toggle target npc body with a outline")]
        public cakeslice.Toggle[] toggleObject;
        [Tooltip("target npc body to interract with")]
        public GameObject target;
        public GameObject questUI;

       
        public bool SetTalk()
        {
            done = true;
            return done;
        }

        private void Start()
        {
            if (!questUI.activeInHierarchy)
            {
                questUI.SetActive(true);
            }
            PlayerPrefs.SetInt("curret_amout_quest_" + quest_id, current_amount);
        }
       

        public void AddNumber()
        {

            if (!questUI.activeInHierarchy)
            {
                questUI.SetActive(true);
            }
            else
            {
                if (current_amount < objectif_amount)
                {

                    PlayerPrefs.SetInt("curret_amout_quest_" + quest_id, current_amount);
                    GameObject current = GameObject.Find("currentAmount").gameObject;
                    current.GetComponent<TextMeshProUGUI>().text = current_amount + "";
                    Destroy(this);
                }
                else
                {    
                        Destroy(GameObject.Find("QuestUI"));
                 
                    
                    if (target.activeInHierarchy == false)
                    {
                        target.SetActive(true);
                        for (int i = 0; i < toggleObject.Length; i++)
                        {
                            toggleObject[i].SelectedObjective();
                        }
                    }
                 
                  
                }
            }
        
           
        }

        public override void Act()
        {
            objectif_amount = PlayerPrefs.GetInt("objectif_quest_" + quest_id);
            current_amount = PlayerPrefs.GetInt("curret_amout_quest_" + quest_id);
            current_amount += amount;
            if (SetTalk())
            {
                AddNumber();
            }  
            
        }
    }

}
