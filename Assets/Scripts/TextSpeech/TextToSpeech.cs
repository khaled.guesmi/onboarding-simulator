﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using UnityEngine.Networking;
 

namespace onboard
{

    [RequireComponent(typeof(AudioSource))]
    public class TextToSpeech : MonoBehaviour
    {

        #region properties
       
        [Header("The Audio Properties")]
        //public InputField _InputText;
        public AudioSource _audio;
     
        string sentence;
        string sentence1;
        string sentence2;
        string sentence3;
        #endregion

        #region base methodes
        // Start is called before the first frame update
        void Awake()
        {
            
            _audio = gameObject.GetComponent<AudioSource>();


            sentence = "We are delighted to welcome you. Your portal in the official application contains all the information you need to start your new experience with Accretio."+
"The portal is interactive, so feel free to browse, click, and explore in your onboarding journey."+
"I also welcome you to our small game in your first day in this establishment.";


            sentence1 = "We hope to provide you with an excellent journey in our Game OnBoarding Simulator. In here you'll get to know your coworkers, your manager, you'll get to discover many other features in our company.";
            sentence2 = "We hope to provide you with an excellent journey in our Game OnBoarding Simulator. In here you'll get to know your coworkers, your manager, you'll get to discover many other features in our company.";
            sentence3 = "We are delighted to welcome you. Your portal in the official application contains all the information you need to start your new experience with Accretio." +
    "The portal is interactive, so feel free to browse, click, and explore in your onboarding journey." +
    "I also welcome you to our small game in your first day in this establishment.";
        }
    
        void Start()
        {
        
         TextToSpeechAudio();
 
        
        }
     
        #endregion

      
     

        #region Custom Methodes

        [System.Obsolete]
         IEnumerator NewGetText(string sentence)
         {
            Regex rgx = new Regex("\\s+");

                 string resultRegex = rgx.Replace(sentence, "+");
                 string newurl = "https://api.voicerss.org/?key=87ecc51f50ad4119b3a80834a79b521f&hl=en-us&src=" + resultRegex + "&f=8khz_8bit_mono&c=WAV";
      
                 WWW www = new WWW(newurl);
                 yield return www;

                 _audio.clip = www.GetAudioClip(false, true, AudioType.WAV);
                 _audio.Play();
#if UNITY_EDITOR
                Debug.Log("Audio clip length : " + _audio.clip.length);
                Debug.Log("speach  start");
#endif
           
            float _duration = _audio.clip.length;
            yield return new WaitForSeconds(_duration);


        }

        /*
        public IEnumerator NewGetText(string sentence)
         {
             sentence = "We are delighted to welcome you. Your portal in the official application contains all the information you need to start your new experience with Accretio."+
 "The portal is interactive, so feel free to browse, click, and explore in your onboarding journey"+
 "I also welcome you to ur small game in your first day in this establishment";
             if (sentence != "")
             {

                 Regex rgx = new Regex("\\s+");

                 string resultRegex = rgx.Replace(sentence, "+");
                 string uri = "https://api.voicerss.org/?key=87ecc51f50ad4119b3a80834a79b521f&hl=en-us&src=" + resultRegex + "&f=8khz_8bit_mono&c=WAV";

                 using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
                 {
                     // Request and wait for the desired page.
                     yield return webRequest.SendWebRequest();
                     yield return new WaitForSeconds(4f);
                     if (webRequest.isNetworkError || webRequest.isHttpError)
                     {
 #if UNITY_EDITOR
                         Debug.Log(": Error: " + webRequest.error);
 # endif
                     }
                     else
                     {
                         AudioClip myClip = DownloadHandlerAudioClip.GetContent(webRequest);

                        _audio.clip =  myClip;
                        _audio.Play();
 #if UNITY_EDITOR
                         Debug.Log(":\nReceived: " + webRequest.downloadHandler.text);
                         Debug.Log("Audio clip length : " + _audio.clip.length);
                        Debug.Log("speach  start");
 #endif
                         yield return new WaitForSeconds(2f);

                     }
                 }
             }
             else
             {
 #if UNITY_EDITOR
                 Debug.Log(": NO MESSAGE TEXT PLEASE CHECK ME "  );

 #endif
             }

         }*/

        IEnumerator WaitForSound(float _duration)
        {
            yield return new WaitForSeconds(_duration);
            print("FinishAudio");
             
        }

 
        public void TextToSpeechAudio( )
        {
        
            StartCoroutine(NewGetText(sentence));

            StartCoroutine(NewGetText(sentence1));
            StartCoroutine(NewGetText(sentence2));
            StartCoroutine(NewGetText(sentence3));
        }
        #endregion

    }

}