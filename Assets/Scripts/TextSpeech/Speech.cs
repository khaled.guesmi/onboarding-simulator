﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace onboard
{

    [System.Serializable]
    public class Speech
    {

        public int id;
        public string text;
        public int scene_index;
        public string npc_name;

        private static Speech _inst;
        public static Speech Instance
        {
            get
            {
                if (_inst is null)
                {
                    _inst = new Speech();
                    _inst.Init();
                }
                return _inst;
            }

        }

        private void Init()
        {

        }

        public override string ToString()
        {
            return "id : " + id + " npc : " + npc_name + " Scene : " + scene_index + " Text : \n" + text;

        }



    }

}