﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine.SceneManagement;


namespace onboard
{

    public class TextLoader : MonoBehaviour
    {
        #region properties
        private static TextLoader _instance;
        public static List<Speech> speaches;
        public int scene_index = 1;
        bool showOnce = true;
        [SerializeField]
        string WEB_URL = "";

        #endregion
        void Awake()
        {
            WEB_URL = "http://localhost/OnboardWS/LoadDiag.php?scene_index=";
            speaches = new List<Speech>();
        }
        private void Start()
        {
            StartCoroutine(Get(WEB_URL));
            if (showOnce)
            { 
                showOnce = false;
            }
        }

   
   
        #region Singletons
        public static TextLoader Instance
        {

            get
            {

                if (_instance == null)
                {
                    _instance = FindObjectOfType<TextLoader>();
                    if (_instance == null)
                    {
                        GameObject go = new GameObject();//create an empty gameobject 
                        go.name = typeof(TextLoader).Name;//modify name 
                        go.AddComponent<TextLoader>();// add this script to the new gameobject 
                        DontDestroyOnLoad(go);//dont destroy the game object on load 
                    }
                }
                return _instance;
            }
        }
        #endregion

        #region Custom meths

  
        public IEnumerator Get(string url)
        {
            url += scene_index;
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();
                string error = www.error;

                if (error != null)
                {
     
                }
                else
                {
                    if (www.isDone) //test if he connection is reached 
                    {
                      
                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                  

                        if (JsonResult != "")
                        {
                            Speech[] entities = JsonHelper.getJsonArray<Speech>(JsonResult);
                            int k = 0;
                            foreach (Speech p in entities)
                            {
 
                                speaches.Add(p);
#if UNITY_EDITOR
                                Debug.Log("index : "+k+"  "+p.ToString());
#endif
                                k++;
                            } 
                        }
                        if (JsonResult == "")
                        {
 
                        }


                    }
                }
            }
        }

      

       

        
        #endregion


    }

}