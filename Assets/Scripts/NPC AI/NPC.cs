﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class NPC : MonoBehaviour
    {
        public float sphere_radious;
        private void OnDrawGizmos()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                
  
          
                int j = getIndex(i);
                Gizmos.color = Color.red;

                Gizmos.DrawSphere(GetWayPoint(i), sphere_radious);

                Gizmos.DrawLine(GetWayPoint(i), GetWayPoint(j));
             
            }
            
        }


        public  int getIndex(int i)
        {
            if (i+1 == transform.childCount)
            {
                return 0;
            }
            return i+1;
        }
        public Vector3 GetWayPoint(int i)
        {
            return transform.GetChild(i).position;
        }
    }

}
