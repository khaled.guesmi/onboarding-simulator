﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Soldier : MonoBehaviour
{
     

    private NavMeshAgent _navMeshAgent;

    public GameObject Player;

    public float AttackDistance = 10.0f;

    public float FollowDistance = 20.0f;

    public Transform[] patrolPoints;

    private int currentControlPointIndex = 0;


    void Awake()
    {
        

        _navMeshAgent = GetComponent<NavMeshAgent>();

      

        MoveToNextPatrolPoint();

    }

    // Update is called once per frame
    void Update()
    {
        if (_navMeshAgent.enabled)
        {
            float dist = Vector3.Distance(Player.transform.position, this.transform.position);

            bool shoot = false;
            bool patrol = false;
            bool follow = (dist < FollowDistance);

           /* if (follow)
            {
               

                _navMeshAgent.SetDestination(Player.transform.position);
            }*/

            patrol = !follow  && patrolPoints.Length > 0;

            if (!follow && !patrol)
                _navMeshAgent.SetDestination(transform.position);
            

            if (patrol)
            {
                if (!_navMeshAgent.pathPending && 
                    _navMeshAgent.remainingDistance < 0.5f)
                    MoveToNextPatrolPoint();
            }

            

            // TODO: Add a walk animation for patrol

        }
    }


    void MoveToNextPatrolPoint()
    {
        if (patrolPoints.Length > 0)
        {
            _navMeshAgent.destination = patrolPoints[currentControlPointIndex].position;

            currentControlPointIndex++;
            currentControlPointIndex %= patrolPoints.Length;
        }
    }

    public void ShootEvent()
    {
       

        //float random = Random.Range(0.0f, 1.0f);

        // The higher the accuracy is, the more likely the player will be hit
       // bool isHit = random > 1.0f - HitAccuracy;

      
    }

    public void Die()
    {
       /* if (!enabled || !vp_Utility.IsActive(gameObject))
            return;

        if (m_Audio != null)
        {
            m_Audio.pitch = Time.timeScale;
            m_Audio.PlayOneShot(DeathSound);
        }

        _navMeshAgent.enabled = false;

        _animator.SetBool("IsFollow", false);
        _animator.SetBool("Attack", false);

        _animator.SetTrigger("Die");

        Destroy(GetComponent<vp_SurfaceIdentifier>());*/

    }
}
