﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace onboard
{
    public class NPCPathController : MonoBehaviour
    {
        /* [SerializeField]
         NPC npcPath;
         [SerializeField]
         float tolerenceWayPoint = 1f;*/

        /* [SerializeField]
         GameObject npc;*/

        /*
                int currWayPointIndex = 0;

                void Update()
                {
                  PatroleBehaviour();
                }
                public void PatroleBehaviour()
                {
                    Vector3 nextPosition = transform.position;
                    if (npcPath != null)
                    {
                        if (AtWayPoint())
                        {
                            CycleWayPoint();

                        }
                        nextPosition = GetCurrentWayPoint();
                        MoveToPosition(nextPosition);
                    }

                }


                public void MoveToPosition(Vector3 position)
                {
                    transform.Translate(position ,Space.World);
                }
                private bool AtWayPoint()
                {
                    float distanceToWayPoint = Vector3.Distance(transform.position, GetCurrentWayPoint());
                    return distanceToWayPoint < tolerenceWayPoint;
                }


                private void CycleWayPoint()
                {
                    currWayPointIndex = npcPath.getIndex(currWayPointIndex);

                }
                private Vector3 GetCurrentWayPoint()
                {
                    return npcPath.GetWayPoint(currWayPointIndex);
                }
            }*/
        private NavMeshAgent _navMeshAgent;

        private Animator anim;

        public bool interract;

        public string animTrigger;

        public string interractTrig;

        public GameObject Player;
         
        public float waitAtPoint = 0.1f;

        public Transform[] patrolPoints;

        private int currentControlPointIndex = 0;


        void Awake()
        {


            _navMeshAgent = GetComponent<NavMeshAgent>();
            anim = GetComponent<Animator>();


            MoveToNextPatrolPoint();

        }

        // Update is called once per frame
        void Update()
        {
            if (_navMeshAgent.enabled)
            {
                float dist = Vector3.Distance(Player.transform.position, this.transform.position);

              
                bool patrol = false;
                 

              if (interract)
                 {
                  _navMeshAgent.enabled = false;

                 }

                patrol =  patrolPoints.Length > 0;

                if (!patrol)
                    _navMeshAgent.SetDestination(transform.position);
               

                if (patrol)
                {
                    if (!_navMeshAgent.pathPending && _navMeshAgent.remainingDistance < 0.5f)
                        StartCoroutine(MovePath());
                }



                // TODO: Add a walk animation for patrol

            }
        }


        void MoveToNextPatrolPoint()
        {
            if (patrolPoints.Length > 0)
            {
                _navMeshAgent.destination = patrolPoints[currentControlPointIndex].position;
                currentControlPointIndex++;
                currentControlPointIndex %= patrolPoints.Length;
            }
        }

        IEnumerator MovePath()
        {
            anim.SetTrigger(animTrigger);
            _navMeshAgent.enabled = false;
            yield return new WaitForSeconds(waitAtPoint);
            _navMeshAgent.enabled = true;
            MoveToNextPatrolPoint();
        }
        public void ShootEvent()
        {


            //float random = Random.Range(0.0f, 1.0f);

            // The higher the accuracy is, the more likely the player will be hit
            // bool isHit = random > 1.0f - HitAccuracy;


        }

        public void Die()
        {
          

        }

    }
}