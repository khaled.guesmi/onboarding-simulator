﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    [CreateAssetMenu(fileName = "Inventory", menuName = "Custom Data/Inventory Data")]
    public class Inventory : ScriptableObject
    {


        #region Attributes
        [SerializeField] ItemDatabase itemDatabase;
        [SerializeField] List<Item> inventory = new List<Item>();
        #endregion
        #region properties
        public ItemDatabase ItemDatabase { get { return itemDatabase; } }
        #endregion


        public void AddItem(Item item)
        {
            inventory.Add(item);
        }

        public int CheckAmount(Item item)
        {
            //check if the item in the current loop has the same id as our param
            //return the amount 
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].ItemId == item.ItemId)
                {
                    if (inventory[i].AllowMultiple)
                    {
                        return inventory[i].Amount;
                    }
                    else
                    {
                        return 1;
                    }

                }
            }
            return 0;
        }
        public void ModifyItemAmount(Item item, int amount )
        {

            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].ItemId == item.ItemId)
                {
                    if (inventory[i].AllowMultiple)
                    {
                        //parcourir all the items of the same kind and delete them
                        inventory[i].ModifyAmount(-amount);
                        if (inventory[i].Amount <= 0)
                        {
                            inventory.RemoveAt(i);
                        }
                    }
                    else
                    {
                        inventory.RemoveAt(i);
                    }

                    return;
                }
            }
            Item newItem = Extensions.CopyItem(item);
            newItem.ModifyAmount(amount);
            AddItem(newItem);
        }
    }

}
