﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    [CreateAssetMenu(fileName = "ItemDatabase", menuName = "Custom Data/Item Database")]
    public class ItemDatabase : ScriptableObject
    {

        #region attributes
        [SerializeField] List<Item> items = new List<Item>();
        [SerializeField] List<string> itemsNames = new List<string>();
        #endregion

        #region properties
        public List<string> ItemsNames { get { return itemsNames; } }
        #endregion


        public void AddItem(Item newItem)
        {
            items.Add(newItem);
            itemsNames.Add("");
        }

        public Item GetItem(int id)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].ItemId == id)
                {
                    return items[i];
                    //break;
                }
            }
            return null;
        }

    }

}
