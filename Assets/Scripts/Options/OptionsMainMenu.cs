﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace onboard
{
    public class OptionsMainMenu : MonoBehaviour
    {
        //audio Mixer
        public AudioMixer _audio_mixer;
       public void SetVolume (float  amount)
        {
           
            _audio_mixer.SetFloat("volume", amount);
            
            //save the volume
        }

        public void SetQuality(int quality_index)
        {
            QualitySettings.SetQualityLevel(quality_index);
        }

        public void ExitGame()
        {
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
    }

} 