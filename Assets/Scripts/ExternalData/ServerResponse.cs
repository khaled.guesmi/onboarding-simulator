﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace onboard
{
    [System.Serializable]
    public class ServerResponse
    {
        public string success;
        public string message;
        public static ServerResponse _inst;
 
        public static ServerResponse Instance
        {
            get
            {
                if (_inst is null)
                {
                    _inst = new ServerResponse();
                    _inst.Init();
                }
                return _inst;
            }

        }

        private void Init()
        {

        }

        public override string ToString()
        {
            return "server response :" + success + "\nmessage  " + message  ;

        }
    }
}

