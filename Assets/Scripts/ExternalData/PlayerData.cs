﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace onboard
{
    public class PlayerData : MonoBehaviour
    {
        #region properties
        //section 1
        public RawImage image_Profile;
        public TMP_Text p_name;
        public TMP_Text p_family_name;
        public TMP_Text p_poste;
    
        public TMP_Text p_email;
        public TMP_Text p_citation;

        //section 2
        public TMP_Text p_tel ;
        public TMP_Text p_hobbies;
        public TMP_Text p_socialMedia;
        public GameObject tagHolder;
        public GameObject prefab1;
        public GameObject prefab2;
        //section 3
        public string p_facebook="your facebook here";
        public string p_twitter="your twitter link";
        public string p_linkedin="your linked in link here";


        #endregion


        void Awake()
        {
            p_name.text = "new player name";
            p_family_name.text = "StaticStrings.currentPlayer.family_name";
            p_poste.text = " StaticStrings.currentPlayer.poste";
            p_email.text = "StaticStrings.currentPlayer.email";
            p_citation.text = "taticStrings.currentPlayer.citation";
            p_tel.text = "StaticStrings.currentPlayer.tel";
            p_facebook = "StaticStrings.currentPlayer.l_facebook";
            p_linkedin = "StaticStrings.currentPlayer.l_linkedin";
            p_twitter = " StaticStrings.currentPlayer.l_twitter";
       
            
        }
 
        void Update()
        {
            PlayerDATA_SHOW();
        }
      
        public void SetFaceBookText()
        {
            if (!p_socialMedia.gameObject.activeSelf)
            {

                p_socialMedia.gameObject.SetActive(true);
            }
            p_socialMedia.text = p_facebook;
        }
        public void SetTwitterText()
        {
            if (!p_socialMedia.gameObject.activeSelf)
            {

                p_socialMedia.gameObject.SetActive(true);
            }
            p_socialMedia.text = p_twitter;
        }
        public void SetLinkedInText()
        {
            if (!p_socialMedia.gameObject.activeSelf)
            {
  
                p_socialMedia.gameObject.SetActive(true);
            }
            p_socialMedia.text = p_linkedin;
        }

        public void PlayerDATA_SHOW()
        {
            if (StaticStrings.currentPlayer != null)
            {
                p_name.text        = PlayerPrefs.GetString("player_name");
                p_family_name.text = PlayerPrefs.GetString("player_prenom");
                p_poste.text       = PlayerPrefs.GetString("player_poste");
                p_email.text       = PlayerPrefs.GetString("player_email");
                p_citation.text    = PlayerPrefs.GetString("player_citation");
                p_tel.text         = PlayerPrefs.GetString("player_tel");
                p_facebook         = PlayerPrefs.GetString("player_l_facebook");
                p_linkedin         = PlayerPrefs.GetString("player_l_linkedin");
                p_twitter          = PlayerPrefs.GetString("player_l_twitter");

                WWW www = new WWW(PlayerPrefs.GetString("Player_Image"));
                if (www.texture != null)
                {
                    image_Profile.texture = www.texture;
                }
                //create Tags
                if (!isCreated)
                {
                    CreateNewTag();
                }


            }
            else
            {
                p_name.text = "new player name";
                p_family_name.text = "StaticStrings.currentPlayer.family_name";
                p_poste.text = " StaticStrings.currentPlayer.poste";
                p_email.text = "StaticStrings.currentPlayer.email";
                p_citation.text = "taticStrings.currentPlayer.citation";
                p_tel.text = "StaticStrings.currentPlayer.tel";
                p_facebook = "StaticStrings.currentPlayer.l_facebook";
                p_linkedin = "StaticStrings.currentPlayer.l_linkedin";
                p_twitter = " StaticStrings.currentPlayer.l_twitter";
                
                //create tags 
                if (!isCreated)
                {
                    CreateNewTag();
                }
            }
        }

        bool isCreated=false;
        public void CreateNewTag()
        {
            string phrase;
            string[] tags;


            if ( PlayerPrefs.GetString("player_hobbies")!= "" )
            {
                phrase = PlayerPrefs.GetString("player_hobbies");  
                tags   = phrase.Split(' ');
                #region tag generation process
                foreach (string x in tags)
                {
                    #region tag instantiate
                    if (x != "")
                    {

                        int range = UnityEngine.Random.Range(0, 10);
                        if (range % 2 == 0)
                        {
                            GameObject p = Instantiate(prefab1, transform.position, Quaternion.identity);

                            TextMeshProUGUI t = p.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                            t.text = x;
                            p.name = x;
                            p.transform.SetParent(tagHolder.transform);
                            RectTransform rt = p.GetComponent(typeof(RectTransform)) as RectTransform;
                            rt.sizeDelta = new Vector2(x.Length, 20);
                        }
                        else
                        {

                            GameObject p = Instantiate(prefab2, transform.position, Quaternion.identity);

                            TextMeshProUGUI t = p.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                            t.text = x;
                            p.name = x;
                            p.transform.SetParent(tagHolder.transform);
                            RectTransform rt = p.GetComponent(typeof(RectTransform)) as RectTransform;
                            rt.sizeDelta = new Vector2(x.Length, 20);

                        }
                    }
 



                    #endregion
                    isCreated = true;
                }

                #endregion
            }
            else
            {
                //string phrase = StaticStrings.currentPlayer.hobbies;
                phrase = "test1 test2 test3 test4 test5";
                tags = phrase.Split(' ');

                #region tag generation process
                foreach (string x in tags)
                {
                    #region tag instantiate
                    if (x != "")
                    {

                        int range = UnityEngine.Random.Range(0, 10);
                        if (range % 2 == 0)
                        {
                            GameObject p = Instantiate(prefab1, transform.position, Quaternion.identity);

                            TextMeshProUGUI t = p.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                            t.text = x;
                            p.name = x;
                            p.transform.SetParent(tagHolder.transform);
                            RectTransform rt = p.GetComponent(typeof(RectTransform)) as RectTransform;
                            rt.sizeDelta = new Vector2(20, 15);
                        }
                        else
                        {

                            GameObject p = Instantiate(prefab2, transform.position, Quaternion.identity);

                            TextMeshProUGUI t = p.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                            t.text = x;
                            p.name = x;
                            p.transform.SetParent(tagHolder.transform);
                            RectTransform rt = p.GetComponent(typeof(RectTransform)) as RectTransform;
                            rt.sizeDelta = new Vector2(x.Length, 20);

                        }
                    }
                    else
                    {
#if UNITY_EDITOR
                        Debug.Log(" no tags : ");
#endif
                    }



                    #endregion
                    isCreated = true;
                }

                #endregion
            }
         

           


        }

        public void GotOFB()
        {
            p_socialMedia.text = "https://www.facebook.com/" + PlayerPrefs.GetString("player_l_facebook");
            Application.OpenURL("https://www.facebook.com/"+PlayerPrefs.GetString("player_l_facebook"));
        }
        public void GoToTwitter()
        {
            p_socialMedia.text = "https://twitter.com/" + PlayerPrefs.GetString("player_l_twitter");
            Application.OpenURL("https://twitter.com/" + PlayerPrefs.GetString("player_l_twitter"));
        }
        public void GoToLinkedIn()
        {
            p_socialMedia.text = "https://www.linkedin.com/in/" + PlayerPrefs.GetString("player_l_linkedin");
            Application.OpenURL("https://www.linkedin.com/in/" + PlayerPrefs.GetString("player_l_linkedin"));
        }



    }

}
