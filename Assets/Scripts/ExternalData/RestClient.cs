﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine.SceneManagement;

namespace onboard
{
    public class RestClient : MonoBehaviour
    {

        #region Properties
        private static RestClient _instance;
        bool showOnce = true;
        private string WEB_URL_Get = "http://localhost/OnboardWS/player.php";
        private string WEB_URL_Post = "http://localhost/OnboardWS/UpdatePlayer.php?id=";
 
       
        #endregion

        #region default meths

         
        void Start()
        {
 

          StartCoroutine(RestClient.Instance.GetPlayers(WEB_URL_Get));

        }


        void Update()
        {
            //StartCoroutine(RestClient.Instance.Get(WEB_URL));

            if (showOnce)
            {
               
                GetPlayers();
                showOnce = false;
            }

        }
        #endregion




        #region Singletons
        public static RestClient Instance
        { 
            get
            {

                if (_instance == null)
                {
                    _instance = FindObjectOfType<RestClient>();
                    if (_instance == null)
                    {
                        GameObject go = new GameObject();//create an empty gameobject 
                        go.name = typeof(RestClient).Name;//modify name 
                        go.AddComponent<RestClient>();// add this script to the new gameobject 
                        DontDestroyOnLoad(go);//dont destroy the game object on load 
                    }
                }
                return _instance;
            }
        }
        #endregion


        #region Custom meths

        public IEnumerator GetPlayers(string url)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError)
                {
#if UNITY_EDITOR
                    Debug.Log("Error Network : " + www.isNetworkError);
#endif
                }
                else
                {

                    if (www.isDone) //test if he connection is reached 
                    {   
                    
                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);


                        if (JsonResult != "")
                        {
                            Player[] entities = JsonHelper.getJsonArray<Player>(JsonResult);

                            foreach (Player p in entities)
                            {
                                 
                                PlayerList.players.Add(p);

                            }
                            StaticStrings.currentPlayer = PlayerList.players[0];
                         
                        }


                    }
                }
            }
        }

        public IEnumerator Set()
        {

 
             WEB_URL_Post += "1&tel="+ StaticStrings.currentPlayer.tel+"&email="+ Converter.RemoveSpecialCharacters(StaticStrings.currentPlayer.email) +"&birthday="+ StaticStrings.currentPlayer.birthday+ "&l_facebook="+ StaticStrings.currentPlayer.l_facebook + "&l_twitter="+ StaticStrings.currentPlayer.l_twitter + "&l_linkedin="+ StaticStrings.currentPlayer.l_linkedin + "&hobbies="+ StaticStrings.currentPlayer.hobbies +"& citation="+ Converter.RemoveSpecialCharacters(StaticStrings.currentPlayer.citation) ;

#if UNITY_EDITOR
            Debug.Log("this is current user " + StaticStrings.currentPlayer.ToString() + "\n");
#endif
           

            using (UnityWebRequest www = UnityWebRequest.Get(WEB_URL_Post))
            {
                yield return www.SendWebRequest();
                if (www.isNetworkError)
                {
#if UNITY_EDITOR
                    Debug.Log("log error " + www.error.ToString());
#endif
             
                }
                else
                {

                    if (www.isDone) 
                    {
                        PlayerSavedData.instance.SavePlayerData();
                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                      
                          ServerResponse[] entities = JsonHelper.getJsonArray<ServerResponse>(JsonResult);
                          ServerResponse sr = new ServerResponse();
                          sr = entities.First();
#if UNITY_EDITOR
                        Debug.Log("response " + sr.message);
#endif  
                        if (sr.message == "success of update" )
                            {
                            PlayerIDCard.Instance.WaitPannel.SetActive(false);
                            }
                            else
                            {
#if UNITY_EDITOR
                           Debug.Log("error setting player "+sr.message);
#endif

                        }


                    }
                }
            }
        }
        void GetPlayers()
        {

            foreach (Player player in PlayerList.players)
            {
                //TODO: assign each data to the right user 
                string playerinfo = "Player : {";
                playerinfo += "player id : " + player.id + "\n";
                playerinfo += "Player name is : " + player.name + "\n";
                playerinfo += "Player familyname is : " + player.family_name + "\n";
                playerinfo += "Player poste is : " + player.poste + "}\n";

                //Debug.Log(playerinfo);

            }
        }

        public void PostePlayerData()
        {
             
            StartCoroutine(Set());
        }
        #endregion
       

    }

}
