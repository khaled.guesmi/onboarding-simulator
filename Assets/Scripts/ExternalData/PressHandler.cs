﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Events;

namespace onboard
{
	public class PressHandler : MonoBehaviour 
	{
		 
	 
		 
        public KeyCode pressedKey;
		public int button_Index;
        public bool useMouse = false;
        [SerializeField]
        Actions[] actions;
       
       
        void Update()
		{
            if (useMouse)
            {
                if (Input.GetMouseButtonDown(button_Index))
                {

                    for (int i = 0; i < actions.Length; i++)
                    {
                        actions[i].Act();
                    }

                }
            }
            else
            {
                if (Input.GetKeyUp(pressedKey))
                {

                    for (int i = 0; i < actions.Length; i++)
                    {
                        actions[i].Act();
                    }

                }
                else if (Input.GetKeyDown(pressedKey))
                {

                    for (int i = 0; i < actions.Length; i++)
                    {
                        actions[i].Act();
                    }

                }
            }
			
            

        }
	}
}
