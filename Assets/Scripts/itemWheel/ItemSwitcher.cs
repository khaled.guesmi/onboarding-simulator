﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace onboard
{

    public class ItemSwitcher : MonoBehaviour
    {

        #region properties
        [SerializeField] private Item1[] Items = new Item1[8];

        private int m_CurrentItemIndex = 0;
        private Item1 m_CurrentItem;

        public Item1 CurrentItem => m_CurrentItem;

        #endregion
        #region instance
        public static ItemSwitcher instance;
        #endregion
        #region custom functions
        public void SwitchItems(int index)
        {
            //Sets our current Weapon
            if (index > Items.Length)
            {
#if UNITY_EDITOR
  Debug.LogError("You are trying to assign the Current weapon to a Non-Existing Weapon!");
#endif
              
                return;
            }
            m_CurrentItemIndex = index;
            for (int i = 0; i < Items.Length; ++i)
            {
                if (Items[i] == null)
                    break;
                if (i != m_CurrentItemIndex)
                {
                    //Disable item
                  Items[i].gameObject.SetActive(false);
                }
                else
                {
                    //Enable item
                   Items[i].gameObject.SetActive(true);
                    m_CurrentItem = Items[i];
                }
            }
        }
        #endregion
        #region default functions
        void Start()
        {
            instance = this;
            m_CurrentItemIndex = 0;
            m_CurrentItem = Items[0];
            SwitchItems(0);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                SwitchItems(0);
            else if (Input.GetKeyDown(KeyCode.Alpha2))
                SwitchItems(1);
            else if (Input.GetKeyDown(KeyCode.Alpha3))
                SwitchItems(2);
            else if (Input.GetKeyDown(KeyCode.Alpha4))
                SwitchItems(3);
            else if (Input.GetKeyDown(KeyCode.Alpha5))
                SwitchItems(4);
            else if (Input.GetKeyDown(KeyCode.Alpha6))
                SwitchItems(5);
            else if (Input.GetKeyDown(KeyCode.Alpha7))
                SwitchItems(6);
            else if (Input.GetKeyDown(KeyCode.Alpha8))
                SwitchItems(7);
            else if (Input.GetKeyDown(KeyCode.Alpha9))
                SwitchItems(8);
        }
        #endregion
        // Start is called before the first frame update

    }
}
