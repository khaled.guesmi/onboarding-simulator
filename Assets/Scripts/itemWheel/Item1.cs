﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace onboard
{

    public class Item1 : MonoBehaviour  
    {
        #region properties

        public int index;
        public new string name;
        public int stack=0;
        public int storeSize;
        
        #endregion
        #region custom functions
        public void AddItemToStack(int amount)
        {
            if (stack < storeSize)
            {
                if (stack+amount > storeSize)
                {
                    stack = storeSize;
                }
                else
                {
                    stack += amount;
                }
            }
            else
            {
#if UNITY_EDITOR
      Debug.Log("can't add more ");
#endif
          
                stack = storeSize;
            }
        }
        public void UseItemFromStack(int amount)
        {
           
            if (stack > 0)
            {
                if (stack - amount < 0)
                {
                    stack = 0;
                }
                else 
                {
                    stack -= amount;
                }    
            }
            else
            {
                stack = 0;
            }
        }
        #endregion
        #region default functions
        public Item1(int index = 0, string name = "Pistol", int stack = 1 , int storeSize=10)
        {
            this.index = index;
            this.name = name;
            this.stack = stack;
            this.storeSize = storeSize;
        }
   


        void Update()
        {
            //stack can't be more than store size 

            if (storeSize - stack < 0)
            {
                stack = storeSize;
            }
        }
        #endregion


    }
}

