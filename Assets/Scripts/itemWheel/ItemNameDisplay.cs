﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace onboard
{
    public class ItemNameDisplay : MonoBehaviour
    {
        #region properties
        private TextMeshProUGUI _text;
        #endregion
        #region custom functions

        #endregion
        #region default functions
        void Start()
        {
            _text = GetComponent<TextMeshProUGUI>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_text == null || ItemSwitcher.instance == null || ItemSwitcher.instance.CurrentItem == null)
                return;

            _text.text = ItemSwitcher.instance.CurrentItem.name;

        }
        #endregion

    }

}
