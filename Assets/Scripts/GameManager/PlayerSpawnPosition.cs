﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace onboard
{
    public class PlayerSpawnPosition : MonoBehaviour
    {
        #region attributes 
        [SerializeField] List<Spawner> spawnEntries = new List<Spawner>();//spawner custom class

        private Transform player;//player position on the map 

        #endregion

        #region deafault methodes 
        void Awake()
        {
            //set player up and cash it 
            player = GameObject.FindGameObjectWithTag("Player").transform;//search for player transform 
        }
        void Start()
        {
          
            Reposition();//reposition the player  to new spawn position
        }
        #endregion

        #region Custum methodes 
        void Reposition()
        {
            for (int i = 0; i < spawnEntries.Count; i++) // loop through all spawner position 
            {
                if (DataManager.Instance.PrevSceneName == spawnEntries[i].PrevSceneName) // check the name of previous Scene Name
                {
                    player.position = spawnEntries[i].SpawnPos;
                    player.rotation = Quaternion.LookRotation(spawnEntries[i].SpawnDir);
                }
            }
        }
        #endregion
    }

    #region custom class 
    [System.Serializable]
    public class Spawner
    {
        [SerializeField] string prevSceneName;//so we take the name of previous scene to spawn in 
        [SerializeField] Vector3 spawnPos;
        [SerializeField] Vector3 spawnDir;

        public string PrevSceneName { get { return prevSceneName; } }
        public Vector3 SpawnPos { get { return spawnPos; } }
        public Vector3 SpawnDir { get { return spawnDir; } }
    }

    #endregion

}
