﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace onboard
{
    public class LevelManager : MonoBehaviour
    {
        //this class to load a new scene and get it by name when I interract with an object like a door 

        #region Instance
        public static LevelManager Instance { get; private set; }
        #endregion


        #region properties
        [SerializeField] GameObject panel;
        [SerializeField] RectTransform loadBar;
        [SerializeField][Tooltip("the periode to wait until scene loaded")]float wait_bar = 2f;
        private Vector3 barScale = Vector3.one;
        #endregion


        #region instantiate class
        private void Awake()
        {
            Instance = this;
 
           
        }
        void Start()
        {
            HidePanel();
        }
        #endregion


        #region Custom methodes

        public void SceneLoad(string sceneName)
        {
 
            StartCoroutine(AsyncLoading(sceneName));
        }

        IEnumerator AsyncLoading(string sceneName)
        {
            ShowPanel();

            yield return new WaitForEndOfFrame();

            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
          
          
            while (!asyncLoad.isDone  )
            {
                  float progress = Mathf.Clamp01(asyncLoad.progress / 0.8f);

                UpdateBar(progress);
                yield return null;
            }
            if (asyncLoad.isDone)
            {
                yield return new WaitForSeconds(wait_bar);
                HidePanel();
            }
           
        }

        void UpdateBar(float value)
        {
            barScale.x = value;

            loadBar.localScale = barScale;

        }

        void ShowPanel()
        {
            panel.SetActive(true);
        }

        void HidePanel()
        {
            panel.SetActive(false);
        }
        #endregion



    }

}
