﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Runtime.Remoting.Messaging;
using UnityEngine.UI;

namespace onboard
{
    public class TutorialManager : MonoBehaviour
    {
        public GameObject[] popups;
        bool isNext = false;
        [SerializeField]
        private int popUpIndex;

        public int PopUpIndex { get => popUpIndex; set => popUpIndex = value; }
        [Header("UI TuTo")]
        public TMP_Text nameText;
        public TMP_Text dialogueText;


        [Header("audio clips")]
        public List<AudioClip> tutorialClips = new List<AudioClip>();

        private static TutorialManager _i;

        public static TutorialManager _I { get
            {
                if (_i is null)
                {
                    _i = new TutorialManager();
                    _i.Init();
                }
                return _i;
            }
        }

       
        private void Init()
        {

        }
        void Update()
        {
            tutorialPopUps();
        }

        public void tutorialPopUps()
        {

            for (int i = 0; i < popups.Length; i++)
            {
                if (i == popUpIndex)
                {
                    popups[popUpIndex].SetActive(true);
                 
                }
                else
                {
                    popups[i].SetActive(false);
                }
            }


            

            if (popUpIndex == 0)
            {
                
                popUpIndex++;
                PlaySound(popUpIndex);

            }
        }


        public  void PlaySound(int index)
        {
             
            GameObject soundGameObject = new GameObject("Sound_GRP");
            AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();

            if (index != 0)
            {

                audioSource.PlayOneShot(tutorialClips[index]);
            }
        }


        IEnumerator TypeSentence(string sentence)
        {
            dialogueText.text = "";
            foreach (char letter in sentence.ToCharArray())
            {
                dialogueText.text += letter;
                yield return null;
            }
        }

        void EndDialogue()
        {
           
            Debug.Log(("end diag"));
        }

        //1 check just if it's empty without advancing (activate btn to confirm) 
        public void MediaInputCheck(TMP_InputField tmp_F)
        {
            string fieldText = tmp_F.text;
            if (fieldText == "") {
                PlaySound(14); // input field is empty so play failed attempt 
                
                Debug.Log("is empty : " + popUpIndex);
            }
            else
            {
                //in case input field isn't empty

                tmp_F.interactable = false; //dissable this field
 
                Debug.Log("is not empty : " + popUpIndex);

                PlaySound(15);//confirm

                isNext = true;
            }
        }

        //2 btn confirm take index and use it ,  index ++;
        public void ActivateConfirmMedia(Button btn)
        {
            if (isNext)
            {
                btn.interactable = true;
                NextIndex();
                
            }

        }
        //check if input field empty then move to another field
        public void IsEmptyField(TMP_InputField tmp_F )
        {
            string fieldText = tmp_F.text;

            if (fieldText == "")
            {
               
                PlaySound(14); // input field is empty so play failed attempt 
                popUpIndex = NextIndex(); //if empty don't go next step
                Debug.Log("is empty : "+popUpIndex);
            }
            else
            {
                //get input text and save it in static class variable 
                //in case input field isn't empty

                tmp_F.interactable = false; //dissable this field
                 
                isNext = true; //go next step

                popUpIndex = NextIndex();//get new index

                Debug.Log("is not empty : " + popUpIndex);

                PlaySound(popUpIndex);//start new sound

              //  isNext = false;
            }
        }
         
        //btn interractable is true , is next is false
        public void ActivateButton(Button btn)
        {
            if (isNext)
            {
                btn.interactable = true;
                isNext = false;
            }
            else
            {
                btn.interactable = false;
            }
        }
        public void ActivateImageTutorial(GameObject img)
        {
         
            if (isNext)
            {
               
                img.SetActive(true);
            }
            else
            {
                img.SetActive(false);
            }
        }
        public int NextIndex()
        {
            if (isNext)
            {
                 Debug.Log(popUpIndex++);
                return popUpIndex++;
            }
            Debug.Log(popUpIndex);
            return popUpIndex;
        }
        public void ActivateNextField(TMP_InputField next)
        {
            if (isNext)
            {
                
                next.interactable = true;
                isNext = false;
            }
        }
    }

}
