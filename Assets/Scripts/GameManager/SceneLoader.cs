﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace onboard
{
    public class SceneLoader : MonoBehaviour
    {

        Scene activeScene;


        void Awake()
        {
            activeScene = SceneManager.GetActiveScene();
        }
        void Start()
        {
            
            StaticStrings.sceneIndex = activeScene.buildIndex;
        }


        public void LoadNextScene()
        {


            var nextscene = activeScene.buildIndex + 1;
            SceneManager.LoadScene(nextscene);
        }
    }

}
