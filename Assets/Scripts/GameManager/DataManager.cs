﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class DataManager : MonoBehaviour
    {

        #region Attributes
        [Tooltip("you need to assig a scriptable object of type Inventory here !")]
        [SerializeField]
        Inventory inventory;

        public event System.Action OnSave = delegate { };
        public event System.Action OnLoad = delegate { };

        public string PrevSceneName { get; private set; }
        public LevelManager LevelManager { get; private set; }

        private int saveDataId = 0;
         
        #endregion
        #region properties
        public static DataManager Instance { get; private set; }
        public Inventory Inventory { get { return inventory; } }
        #endregion


        #region default methodes 

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
            LevelManager = GetComponentInChildren<LevelManager>();
        }

        #endregion

        #region custom methodes 
    
        public void SetPrevScene(string name)
        {
            PrevSceneName = name;
        }
        #endregion

    }
}

