﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace onboard
{
    public class Interraction : MonoBehaviour
    {
        #region properties
        [SerializeField]
        float distance = 1.0f;
 
        public Actions[] actions;

      //  [SerializeField]
        //Sprite spriteCursor;
     //   [SerializeField] bool lookOnly;
       // public bool LookOnly { get { return lookOnly; } }
       // public Sprite SpriteCursor { get { return spriteCursor; } }


        public cakeslice.Toggle[] toggleObject;
        
        int j = 0;

        #endregion


        #region custom meths
        private void Reset()
        {
            gameObject.layer = LayerMask.NameToLayer("Interactable");
        }

        public void SetActions(Actions a)
        {

#if UNITY_EDITOR
            Debug.Log("added");
#endif
            actions.ToList().Add(a);
            
        }
        public Vector3 InterractPosition()
        {
            return transform.position + transform.forward * distance;
        }
        public void Interract(JoystickPlayerInput player)
        { 
            StartCoroutine(WaitForPlayerToReach(player)); 
        }


        public IEnumerator WaitForPlayerToReach(JoystickPlayerInput player)
        {
           
            while (!player.CheckDestinationReach())
            {
                yield return null;
            }
            for (int i = 0; i <  toggleObject.Length; i++)
            {
                 toggleObject[i].SelectedObjective();
            }
#if UNITY_EDITOR
            Debug.Log("this interracted "+gameObject.name + " player arrived");
#endif
         
            player.FaceTarget(transform.position); //when we arrive
             
            if (actions.Length > 0)
                {
                    foreach (Actions action in actions)
                    {
                        action.Act();
                    }
                
            }  
          


        }
        #endregion

    }
}

