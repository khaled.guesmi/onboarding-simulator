﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class Game : MonoBehaviour
    {

        #region Properties

        PlayerList playerList;

        string WEB_URL = "http://localhost/OnboardWS/player.php";
        //public string speech_url = "http://localhost/OnboardWS/textToSpeak.php";

        #endregion
        void Start()
        {
            StartCoroutine(RestClient.Instance.GetPlayers(WEB_URL));
        }

     

        #region Custom Methodes
        public Player GetPlayer()
        {


            return PlayerList.players[0];

        }
        public void PosteInfoPlayer()
        {
            RestClient.Instance.PostePlayerData();
        }
        #endregion


    }

}
