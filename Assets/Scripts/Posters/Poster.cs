﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace onboard
{
   public class Poster : MonoBehaviour
    {
        #region props
        public Texture[] myTextures = new Texture[5];
        int maxTextures;
        int arrayPos = 0;
        #endregion


        #region default methodes
             // Start is called before the first frame update
             void Awake()
        {
            GetComponent<Renderer>().material.mainTexture = myTextures[0];
        }
        void Start()
        {
             maxTextures = myTextures.Length-1;
              
        }

        // Update is called once per frame
        void Update()
        {
                if (Input.GetKeyDown(KeyCode.U))
             {
                 GetComponent<Renderer>().material.mainTexture = myTextures[arrayPos];
 
                 if(arrayPos == maxTextures)
                 {
                     arrayPos = 0;
                 }
                 else
                 {
                     arrayPos++;
                 }
             }
        }
        #endregion


    }
}

