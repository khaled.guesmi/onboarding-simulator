﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace onboard
{

    [System.Serializable]
    public class ImageList
    {
        #region Properties
        public static List<Image> images = new List<Image>();

        #endregion
    }


}