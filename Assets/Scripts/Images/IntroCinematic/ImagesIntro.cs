﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine.SceneManagement;




namespace onboard
{
        public class ImagesIntro : MonoBehaviour
        {
            #region properties

            [SerializeField]
            private string urli = "http://localhost:8080/OnboardWS/ImageLoader.php?sceneNB=";
            private static ImagesIntro _instance;
            int sceneNB;
            Scene activeScene;
            bool shownOnce = false;
            public RawImage rimg1;
            public RawImage rimg2;
            public RawImage rimg3;

            #endregion

            #region Singletons
            public static ImagesIntro Instance
            {

                get
                {

                    if (_instance == null)
                    {
                        _instance = FindObjectOfType<ImagesIntro>();
                        if (_instance == null)
                        {
                            GameObject go = new GameObject();//create an empty gameobject 
                            go.name = typeof(ImagesIntro).Name;//modify name 
                            go.AddComponent<ImagesIntro>();// add this script to the new gameobject 
                            DontDestroyOnLoad(go);//dont destroy the game object on load 
                        }
                    }
                    return _instance;
                }
            }
            #endregion



            #region default meths
            private void Start()
            {

                activeScene = SceneManager.GetActiveScene();
                sceneNB = activeScene.buildIndex;
                urli += sceneNB;
                StartCoroutine(ImagesIntro.Instance.Get(urli));
                ImportImageList();
            }
            void Update()
            {

                ImportImageList();


            }
            #endregion

            #region Custom Methodes



            public IEnumerator Get(string url)
            {
                using (UnityWebRequest www = UnityWebRequest.Get(url))
                {
                    yield return www.SendWebRequest();
                    if (www.isNetworkError)
                    {
#if UNITY_EDITOR
Debug.Log(www.error + "\nNetwork connection failed");
#endif
                    
                    }
                    else
                    {
                        if (www.isDone) //test if he connection is reached 
                        {
                            string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

                            if (JsonResult != "")
                            {
                                Image[] entities = JsonHelper.getJsonArray<Image>(JsonResult);

                                foreach (Image p in entities)
                                {
                                    ImageList.images.Add(p);

                                }

                            }


                        }
                    }
                }
            }
            public void ImportImageList()
            {



                /*string link1 = ImageList.images[0].link;
                string link2 = ImageList.images[1].link;
                string link3 = ImageList.images[2].link;*/

                string link1 = "http://localhost/OnboardWS/imgs/1.jpg";
                string link2 = "http://localhost/OnboardWS/imgs/2.jpg";
                string link3 = "http://localhost/OnboardWS/imgs/3.jpg";
                LoadImageFromUrl(link1, rimg1);
                StopAllCoroutines();
                LoadImageFromUrl(link2, rimg2);
                StopAllCoroutines();
                LoadImageFromUrl(link3, rimg3);
                StopAllCoroutines();
            }
            //load images
            public void LoadImageFromUrl(string url, RawImage raw)
            {

                StartCoroutine(loadImageInUnity(url, raw));

            }

            public IEnumerator loadImageInUnity(string url, RawImage rimg)
            { 
            
                UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError)
                {
#if UNITY_EDITOR
 Debug.Log(www.error);
#endif
               
                }
                else
                {
                    Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                    if (myTexture != null)
                    {
                        rimg.texture = myTexture;
                    }
                

                }

        }
            #endregion

        }


}


//this script is for the cinematic scene to load the background images from the server 