﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Text.RegularExpressions;
using UnityEngine.Networking;

namespace Onboard
{
    public class FolderManager : MonoBehaviour
    {

        #region properties
            string path ;
            public RawImage image ;
        #endregion
   
        #region Custom Meths
     public void OpenExplorer(){
#if UNITY_EDITOR
            path = EditorUtility.OpenFilePanel("Overrite with png", "", "png");

            GetImage();
#endif

        }
    void GetImage(){
         
             StartCoroutine("UploadImage",path);
       
    }

   public IEnumerator UploadImage(string path){
            /*
            WWW www = new WWW("file:///"+path);
             yield return www;
             if (www.error == null && www.texture != null)
             {
             Texture2D texture2D = www.texture;
             image.texture =  texture2D ;
           } else
                {
                    //avatarImage = texNotFound;
                    Debug.Log("Texture not found");
                }*/

            UnityWebRequest www = UnityWebRequestTexture.GetTexture("file:///" + path);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
#if UNITY_EDITOR
   Debug.Log(www.error);
#endif
             
            }
            else
            {
                Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                if (myTexture != null )
                {
                    image.texture = myTexture;
                }
               
            }
        }

        #endregion
   

    }

}
