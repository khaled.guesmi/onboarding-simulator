﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace onboard
{
    [System.Serializable]
    public class Image
    {
        public int id;
        public string link;
        public int scene_name;
        public string npc_name;
        public Color color { get; internal set; }
    }

}