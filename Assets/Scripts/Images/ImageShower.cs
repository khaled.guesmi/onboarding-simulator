﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace onboard
{
    public class ImageShower : MonoBehaviour
    {

        #region properties
        
        public RawImage img; //image Holder
        [HideInInspector]
        public string link;
        public static ImageShower Instance { get; private set; }


        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }
        public IEnumerator loadImageInUnity(string url)
        {

            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                if (myTexture != null)
                {
                    img.texture = myTexture;
                }


            }

        }

    
        #endregion
    }

}
