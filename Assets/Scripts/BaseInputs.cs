﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace onboard
{
 
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(Rigidbody))]
    public class BaseInputs : MonoBehaviour
{
 
    #region properties
    
 
    [Space]
    [Header("Animation Input Prameters ")]
    public float maxSpeed = 10f;

    private float speedx ;
    private float speedy ;        
    Animator _animator ;
  
    GameObject badge ;
    public GameObject badgeGameObject ;
    public float Speedx{ get{ return speedx ;} }
    public float Speedy { get{ return speedy ;} } 
    #endregion
 
        void Awake()
        {
            _animator = gameObject.GetComponent<Animator>();
            badge = GameObject.Find("BadgeFullView");
            if (badge == null)
            {
#if UNITY_EDITOR
                Debug.LogError("no badge found initiating");
#endif

                badge = Instantiate(badgeGameObject);

            }
            else
            {

                badge.transform.gameObject.SetActive(false);
            }

            if (_animator == null)
            {
                _animator = GetComponentInChildren<Animator>();

            }
        }
 
 
    void Update()
    {
        if (_animator == null) 
            MoveNoAnimation() ;

     HandleInput();

    }

#region custom
        protected virtual void HandleInput()
        {
         
        speedx =  Input.GetAxis("Horizontal");
        speedy =  Input.GetAxis("Vertical"); 
         Move(speedx,speedy);
        }
   public  void Move(float x , float y){

        _animator.SetFloat("speedX",x);
        _animator.SetFloat("speedY",y);
        transform.position += Vector3.forward * maxSpeed * y *Time.deltaTime ;
        transform.position += Vector3.right * maxSpeed * x *Time.deltaTime ;
        Debug.DrawLine(transform.position , Vector3.forward , Color.green);
         

    }

 public void MoveNoAnimation(){
      speedx =  Input.GetAxis("Horizontal");
      speedy =  Input.GetAxis("Vertical"); 
       transform.position += Vector3.forward * maxSpeed *speedx *Time.deltaTime ;
       transform.position += Vector3.right * maxSpeed *speedy *Time.deltaTime ;
        Debug.DrawLine(transform.position , Vector3.forward , Color.green);
 }

 
private void OnCollisionEnter(Collision other) {
#if UNITY_EDITOR
  Debug.Log("other collided with me " + other.gameObject.tag);
#endif
          
}
private void OnTriggerEnter(Collider other) {
#if UNITY_EDITOR
 Debug.Log("other triggered with me object" + other.tag);
#endif
           
    if (other.tag == "Badge")
    {
        badge.transform.gameObject.SetActive(true);
        
        if (badge == null)
        {
#if UNITY_EDITOR
         Debug.LogError("couldn't find badge camera ");
#endif
           
        } 
      
    }
    //add badge to 
}

private void OnTriggerExit(Collider other) {
    if (other.tag == "Badge")
    {
        if (badge == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("couldn't find badge camera ");
#endif

        } 
        badge.transform.gameObject.SetActive(false);
#if UNITY_EDITOR
                Debug.Log("Exit Badge view mode");
#endif

       
      
          //add badge to inventory
    }
}
#endregion

}

}
  /* string m_MyValue;
    public string Speed
    {
        get { return m_MyValue; }
        set { m_MyValue = value; }
    }*/ 
       // [SerializeField]
   // [Space]
  //  [Tooltip("Speed value between 0 and 1 determens what type of move animation player will do 0 : idle 0.5 : walking 1: running.")]
  //  private float walkspeed = 0f ;

  //  [SerializeField]
  //  [Space]
 //   [Tooltip("Jump Speed value between 0 and 1 determens what type of jump animation player will do 0 :   0.5 :   1:  .")]
  //  private float jumpSpeed = 0f ;
//    [SerializeField]
 //   [Space]
 //   [Tooltip("saluting value between 0 and 1 determens what type of saluting animation player will do 0 :   0.5 :   1:  .")]
 //   private float saluting = 0f ;
 //   [SerializeField]
 //   [Space]
 //   [Tooltip("sitting value between 0 and 1 determens what type of sitting animation player will do 0 :   0.5 :   1:  .")]
//    private float sitting = 0f ; 
  /* public float MaxSpeed { get{ return maxSpeed ;} }
    public float Walkspeed { get{ return walkspeed ;} } 
    public float JumpSpeed { get{ return jumpSpeed ;} }   
    public float Saluting { get{ return saluting ;} }
    public float Sitting { get{ return sitting ;} } */ 