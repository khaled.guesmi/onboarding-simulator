﻿using UnityEngine;


namespace onboard
{
    public class Network : MonoBehaviour
    {
        public static Network Instance;
        private void Start()
        {
            if (Instance == null)
            {
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this);
            }
            
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
#if UNITY_EDITOR
                Debug.Log("Error. Check internet connection!");
#endif

            }
            else
            {
#if UNITY_EDITOR
                Debug.Log("connected!");
#endif

            }
        }

    }
}

