﻿using onboard;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class PlayerSavedData : MonoBehaviour
    {
        bool isSet = false;
        public static PlayerSavedData instance;
        void Start()
        {
            if (instance == null)
            {
                instance = this;
            }
        }
       /* void Update()
        {
            if (!isSet)
            {
                SavePlayerData();
            }
           
         
        }*/

        public void SavePlayerData()
        {
            PlayerPrefs.SetInt("player_id", StaticStrings.currentPlayer.id);
            PlayerPrefs.SetString("player_name", StaticStrings.currentPlayer.name);
            PlayerPrefs.SetString("player_prenom", StaticStrings.currentPlayer.family_name);
            PlayerPrefs.SetInt("player_badge_id", StaticStrings.currentPlayer.badge_id);
            PlayerPrefs.SetString("player_poste", StaticStrings.currentPlayer.poste);
            PlayerPrefs.SetString("player_tel", StaticStrings.currentPlayer.tel);
            PlayerPrefs.SetString("player_email", StaticStrings.currentPlayer.email);
            PlayerPrefs.SetString("player_birthday", StaticStrings.currentPlayer.birthday);
            PlayerPrefs.SetString("player_l_facebook", StaticStrings.currentPlayer.l_facebook);
            PlayerPrefs.SetString("player_l_twitter", StaticStrings.currentPlayer.l_twitter);
            PlayerPrefs.SetString("player_l_linkedin", StaticStrings.currentPlayer.l_linkedin);
            PlayerPrefs.SetString("player_hobbies", StaticStrings.currentPlayer.hobbies);
            PlayerPrefs.SetString("player_citation", StaticStrings.currentPlayer.citation);
            PlayerPrefs.SetString("player_accepted_turms", StaticStrings.currentPlayer.accepted_turms);
            isSet = !isSet;
        }

    }
}

