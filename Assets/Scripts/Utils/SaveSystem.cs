﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;



namespace onboard
{
    public static class SaveSystem  
    {
       
        public static void save<T>(T saveData)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = new FileStream(Application.persistentDataPath + "/savedata.dat",FileMode.Create);
            binaryFormatter.Serialize(file, saveData);
            file.Close();
#if UNITY_EDITOR
            Debug.Log("save success");
#endif
        }
    }
}

