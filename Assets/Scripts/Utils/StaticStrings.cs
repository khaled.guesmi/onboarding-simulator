﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class StaticStrings : MonoBehaviour
    {
        public static StaticStrings _instance;
        public int Instance {  get; private set; }
        public static string nameNPC = "";//StaticStrings.nameNPC
        public static int sceneIndex; //StaticStrings.sceneIndex
        public static Player currentPlayer;//StaticStrings.currentPlaye
        public static Dialogue currentDialogue;//StaticStrings.currentDialogue
        public static Speech speech;//StaticStrings.speech
        public static string building = "building";//StaticStrings.building
        public static string doors = "Door";//StaticStrings.doors
        public static DocumentList allDocuments;//StaticStrings.allDocuments.docs[i]
        void Start()
        {
            currentPlayer = Player.Instance; //instantiate player static value 
            currentDialogue = Dialogue.Instance; //instantiate Dialog class 
            allDocuments = DocumentList.Instance;//instantiate document list
            speech = Speech.Instance;

            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

}
