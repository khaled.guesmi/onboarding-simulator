﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace onboard
{
    public class WoldMapRotate : MonoBehaviour
    {

        [SerializeField]
        Transform targetTransform;
       
      
        RectTransform thisRect;
        float distanceFromCenter;
        bool show = false;
        UnityEngine.UI.Image thisImg;


        void Awake()
        {
            thisRect = GetComponent<RectTransform>();
            thisImg = GetComponent<UnityEngine.UI.Image>();
        }
        void Update()
        {
            var screenPoint = Camera.main.WorldToScreenPoint(targetTransform.position);
            thisRect.position = screenPoint;

            var viewportPoint = Camera.main.WorldToViewportPoint(targetTransform.position);
            distanceFromCenter = Vector2.Distance(viewportPoint, Vector2.one * .5f);
            show = distanceFromCenter < 0.3f;
            thisImg.enabled = show;
        }
    }

}
