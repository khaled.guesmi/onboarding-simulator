﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace onboard
{
    public class PlayerTriggers : MonoBehaviour
    {

        [SerializeField]
        cakeslice.Toggle[] toggleObject;
        #region custom methodes
        private void OnTriggerEnter(Collider other)
        {
            for (int i = 0; i < toggleObject.Count(); i++)
            {
                toggleObject[i].SelectedObjective();
            }
           
        }

        private void OnTriggerExit(Collider other)
        {
            for (int i = 0; i < toggleObject.Count(); i++)
            {
                toggleObject[i].SelectedObjective();
            }
        }
        #endregion
    }

}
