﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public abstract class Actions : MonoBehaviour
    {
        public abstract void Act();
    }

}
