﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
namespace onboard
{
    public class MessageAction : Actions
    {
        #region properties
        [Multiline(5)]
        [SerializeField]
        List<string> messages = new List<string>();
        [SerializeField]
        List<string> tags = new List<string>();
        [SerializeField]
        string yesText, noText;
        [SerializeField]
        bool enableDialog;
        [SerializeField]
        bool enablenext;
        [SerializeField]
        List<Actions> yesActions = new List<Actions>();
        [SerializeField]
        List<Actions> noActions = new List<Actions>();
        [SerializeField]
        string npc_name = "npc";
        [SerializeField]
        Texture m_texture;
        #endregion
        #region properties 2
        public List<string> Messages { get { return messages; } }
        public List<string> Tags { get { return tags; } }
        public List<Actions> YesActions { get { return yesActions; } }
        public List<Actions> NoActions { get { return noActions; } }
     
        /*   public  string YesText { get { return yesText; } }
           public string NoText { get { return noText; } }*/
        public bool EnableDialog { get { return enableDialog; } }
      
        #endregion

        

        public override void Act()
        {
            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                foreach (var e in TextLoader.speaches)
                {
                    messages.Add(e.text);
                }
                StartCoroutine(WaitForServerRESP());
            }
            else
            {
                for (int i = 0; i < messages.Count; i++)
                {
                    messages[i] = GameMultiLang.GetTraduction(tags[i]);
                }
                DialogueSystem.Instance.PrintMessages(messages, enableDialog, yesActions, noActions, yesText, noText , npc_name , m_texture);
              
            }
           
            
        }
        IEnumerator WaitForServerRESP()
        {
            yield return new WaitForSeconds(2f);
        }

    }

}

