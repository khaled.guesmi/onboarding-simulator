﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace onboard
{
    public class ToggleOutlineAction : Actions
    {
        [SerializeField]
        cakeslice.Toggle toggleObject;
       

        public override void Act()
        {
#if UNITY_EDITOR
   Debug.Log("toggle");
#endif
         
            toggleObject.SelectedObjective();
        }
    }

}
