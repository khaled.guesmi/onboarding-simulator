﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace onboard
{
    public class NPCMoveAction : Actions
    {
        //move npc from point to another throw script 

        #region attributes
        [Header("NPC movement regulator")]

        [Tooltip("animation delay action")]
        [SerializeField]
        float delay;

        [Tooltip("NPC coordonation/ Position")]
        [SerializeField]
        Vector3 targetPosition;

        private PlayerAnimation npcAnim;
        private NavMeshAgent agent;
        private bool isMoving;

        #endregion


        #region mathodes 
        void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            npcAnim = new PlayerAnimation();
            npcAnim.Init(GetComponentInChildren<Animator>());
        }
        void Update()
        {
            if (isMoving)
                npcAnim.UpdateAnimation(agent.velocity.sqrMagnitude);

            if (isMoving && agent.remainingDistance <= agent.stoppingDistance)
            {
                isMoving = false;
                npcAnim.UpdateAnimation(0f);
            }

        }
        public override void Act()
        {
            StartCoroutine(MoveNPC(delay));
        }
        //corroutine function
        IEnumerator MoveNPC(float delay)
        {
            yield return new WaitForSeconds(delay);
            //action after delay 
            isMoving = true;
            agent.SetDestination(targetPosition);
        }

        #endregion




    }

}
