﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace onboard
{
    [RequireComponent(typeof(Animator))]
    public class AnimationAction : Actions
    {
        #region properties
        [SerializeField] List<AnimationParams> anims = new List<AnimationParams>();
        [SerializeField] List<Actions> actions = new List<Actions>();

        private Animator animator;
        #endregion

        #region Methodes 
        void Start()
        {
            //instantiate animator 
            animator = GetComponentInChildren<Animator>();
            for (int i = 0; i < anims.Count; i++)
            {
                anims[i].InitHashId(); //initializing the animations 
            }
        }
        public override void Act()
        {
            StartCoroutine(Animate());
        }
        IEnumerator Animate()
        {
            int i = 0;

            while (i < anims.Count)
            {
                yield return new WaitForSeconds(anims[i].InvokeDelay);
                animator.SetTrigger(anims[i].HashID);
                i++;
                yield return null;
            }
            for (int j = 0; j < actions.Count; j++)
            {
                actions[j].Act();
            }
        }
        #endregion

    }


    [System.Serializable]
    public class AnimationParams
    {
        #region properties
        [SerializeField] string triggerName;
        [SerializeField] float invokeDelay;
        public int HashID { get; private set; }
        public float InvokeDelay { get { return invokeDelay; } }
        #endregion


        public void InitHashId()
        {
            HashID = Animator.StringToHash(triggerName);
        }
    }
}
