﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class WelcomeSceneAction : MonoBehaviour
    {

        public Actions[] actions;


        // Update is called once per frame
        void Start()
        {
            StartCoroutine(StartConversation());
        }

        
        public IEnumerator StartConversation()
        {
            yield return new WaitForSeconds(.2f);
            if (actions.Length > 0)
            {
                foreach (Actions action in actions)
                {
                    action.Act();
                }
            }
        }
    }

}
