﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace onboard
{
    public class DeleteActions : Actions
    {

        #region properties 
        [SerializeField]
        Actions[] deleteActionList ;
       [SerializeField]
        Actions[] newActions; 
       
        public Interraction interact;
        
        #endregion


        
        public override void Act()
        {
            if (interact!=null)
            {
                for (int i = 0; i < deleteActionList.Length; i++)
                {

                    interact.actions.ToList().Remove(deleteActionList[i]);


                }
                Destroy(this);
                foreach (Actions action in newActions)
                {
                    action.Act();
                    interact.SetActions(action);
                }
            }
            else
            {
                for (int i = 0; i < deleteActionList.Length; i++)
                {

                    Destroy(deleteActionList[i]);


                }
            }
            
            
        }

        
    }
}