﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace onboard
{
    public class MissionAction : Actions
    {

        #region properties
        [SerializeField]
        [Tooltip("quest prefab")]
        GameObject questUI;
        [SerializeField]
        GameObject parentPanel;
        [SerializeField]
        [Multiline(2)]
        string missionObjectiveText;
        [SerializeField]
        [Multiline(5)]
        string missionDescription;
        [SerializeField]
        int objectif = 10;
        int curret_amout = 0;
        public int id_quest = 0; // must be unique 
     
        #endregion

        public void Start()
        {
            //start the quest 
          

        }
        public override void Act()
        {
            
            PlayerPrefs.SetInt("id_quest_" + id_quest, id_quest);
            PlayerPrefs.SetInt("objectif_quest_" + id_quest, objectif);
            PlayerPrefs.SetInt("curret_amout_quest_" + id_quest, curret_amout);
            PlayerPrefs.SetString("description_quest_" + id_quest, missionDescription);
            PlayerPrefs.SetString("title_quest_" + id_quest, missionObjectiveText);
            questUI.GetComponent<TextMeshProUGUI>().text = missionObjectiveText;
            GameObject description = questUI.transform.Find("QuestDescription").gameObject;
            GameObject current = questUI.transform.Find("currentAmount").gameObject;
            GameObject objtif = questUI.transform.Find("OBjectifAmount").gameObject;
            description.GetComponent<TextMeshProUGUI>().text = missionDescription;
            current.GetComponent<TextMeshProUGUI>().text = curret_amout+"";
            objtif.GetComponent<TextMeshProUGUI>().text = objectif+"";

            //GameObject icon = questUI.transform.Find("QuestDescription").gameObject;
             
            Instantiate(questUI, parentPanel.transform);
            GameObject qUI = GameObject.Find("QuestUI(Clone)");



        }
    }
}

