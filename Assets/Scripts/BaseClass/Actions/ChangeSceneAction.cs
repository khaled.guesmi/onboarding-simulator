﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace onboard
{
    public class ChangeSceneAction : Actions
    {
        [SerializeField] string sceneTarget;

        public override void Act()
        { 
            DataManager.Instance.SetPrevScene(SceneManager.GetActiveScene().name);
            DataManager.Instance.LevelManager.SceneLoad(sceneTarget);
        }
    }

}
