﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class TriggerInteract : MonoBehaviour
    {
        [SerializeField]
        string selectTag;
        [SerializeField]
        Actions[] enterActions;
        [SerializeField]
        Actions[] exitActions;


        #region custom methodes
        private void OnTriggerEnter(Collider other)
        {
 
            if (!other.CompareTag(selectTag))
               return;
            for (int i = 0; i < enterActions.Length; i++)
            {
                enterActions[i].Act();
            }
        }

        private void OnTriggerExit(Collider other)
        {
 
            if (!other.CompareTag(selectTag))
                return;
            for (int i = 0; i < exitActions.Length; i++)
            {
                exitActions[i].Act();
            }
        }
        #endregion
    }
}

