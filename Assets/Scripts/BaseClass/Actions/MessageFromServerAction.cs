﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class MessageFromServerAction : Actions
    {
        [SerializeField]
        List<string> messages;
        [SerializeField]
        Texture npc_profile;
        public List<string> Messages { get { return messages; } }
         


        void Awake()
        {
            messages  = new List<string>();
        }
 
        public override void Act()
        {
            foreach (var e in TextLoader.speaches)
            {
                messages.Add(e.text);
            }
            StartCoroutine(WaitForServerRESP());
            DialogueSystem.Instance.PrintMessages(messages, false , null, null, "yes", "no","RH Manager");
        
        }

        IEnumerator WaitForServerRESP()
        {
         
            yield return new WaitForSeconds(2f);
        }

        
    }


}