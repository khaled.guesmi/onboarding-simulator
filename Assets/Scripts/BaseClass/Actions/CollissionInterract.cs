﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class CollissionInterract : MonoBehaviour
    {
        [SerializeField]
        string selectTag;
        [SerializeField]
        Actions[] enterActions;
        [SerializeField]
        Actions[] exitActions;
         
        void OnCollisionEnter(Collision collision)
        {
#if UNITY_EDITOR
            Debug.Log("you are interracting with  " + collision.transform.name);
#endif
            if (selectTag == "Player")
            {
                for (int i = 0; i < enterActions.Length; i++)
                {
                    enterActions[i].Act();
                }
            }
           

        }
        void OnCollisionExit(Collision collisionInfo)
        {
#if UNITY_EDITOR
            print("No longer in contact with " + collisionInfo.transform.name);
#endif
            if (selectTag == "Player")
            {
                for (int i = 0; i < enterActions.Length; i++)
                {
                    enterActions[i].Act();
                }
            }
        }
    }

}
