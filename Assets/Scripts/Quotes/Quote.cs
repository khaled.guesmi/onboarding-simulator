﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    [System.Serializable]
    public class Quote 
    {
        public string quoteText;
        public string quoteAuthor;
        public string senderName;
        public string senderLink;
        public string quoteLink;


        public override string ToString()
        {
            return "quote : " + quoteText + ",author : " + quoteAuthor;
        }
    }

}
