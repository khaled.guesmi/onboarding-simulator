﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

namespace onboard
{
    public class QuotesGenerator : MonoBehaviour
    {
        #region properties
        string url = "https://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=en";
       public TextMeshProUGUI quotes_Holder;
        #endregion


        Quote quotes;

        void Update()
        {
            StartCoroutine("GetQuote");
        }
        public IEnumerator GetQuote()
        {
            yield return new WaitForSeconds(60f* Time.deltaTime);
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
#if UNITY_EDITOR
                    Debug.Log("Network failing error");
#endif
                }
                else
                {

                    if (www.isDone)
                    {


                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                   
                   
                     
                        if (JsonResult != "")
                        {
                          
                            quotes = JsonUtility.FromJson<Quote>(JsonResult);
                            quotes_Holder.text = quotes.quoteText +"\n   -"+quotes.quoteAuthor+"- ";
                            
                        }
                        else
                        {
 
                        }


                    }
                }
            }
        }
    }

}
