﻿using UnityEngine;
using UnityEngine.UI;
namespace onboard
{
    [System.Serializable]
    public class Item
    {
        #region attributes
        [SerializeField]
        int itemId;
        [SerializeField]
        string itemName;
        [SerializeField]
        string itemDescription;
        [SerializeField]
        Sprite itemSprite;
        [SerializeField]
        bool allowMultiple;
        [SerializeField]
        int amount;

        public int stack = 0;
        public int storeSize;
        #endregion



        public Item(int itemId, string name, string desc, Sprite p, bool allowMultiple)
        {
            this.itemId = itemId;
            this.itemName = name;
            this.itemDescription = desc;
            this.itemSprite = p;
            this.allowMultiple = allowMultiple;
        }

        #region Properties
        public int ItemId { get { return itemId; } }
        public string ItemName { get { return itemName; } }
        public string ItemDescription { get { return itemDescription; } }
        public Sprite ItemSprite { get { return itemSprite; } }
        public bool AllowMultiple { get { return allowMultiple; } }
        public int Amount { get { return amount; } }
        #endregion


        public void ModifyAmount(int value)
        {
            amount += value;
        }

        #region custom functions
        public void AddItemToStack(int amount)
        {
            if (stack < storeSize)
            {
                if (stack + amount > storeSize)
                {
                    stack = storeSize;
                }
                else
                {
                    stack += amount;
                }
            }
            else
            {
#if UNITY_EDITOR
 Debug.Log("can't add more ");
#endif
               
                stack = storeSize;
            }
        }
        public void UseItemFromStack(int amount)
        {

            if (stack > 0)
            {
                if (stack - amount < 0)
                {
                    stack = 0;
                }
                else
                {
                    stack -= amount;
                }
            }
            else
            {
                stack = 0;
            }
        }
        #endregion


    }
}

