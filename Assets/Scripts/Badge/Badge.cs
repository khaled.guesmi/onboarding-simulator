﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;
using System.Text;

namespace onboard
{
    public class Badge : MonoBehaviour
    {


        public TMP_InputField fb;
        public TMP_InputField li;
        public TMP_InputField twt;

        bool isDisabled = false;
 
        public void savebadgeData()
        {
#if UNITY_EDITOR
Debug.Log("save badge");
#endif
            
        }

        public void closeBadgeViewMode()
        {
#if UNITY_EDITOR
  Debug.Log("close  badge");
#endif
          
        }

        public void printPlayerInformation()
        {
#if UNITY_EDITOR
 Debug.Log("badge info ");
#endif
           
        }

        public void addFacebookLink()
        {
            if (fb.text != "")
            {

            
              
                StaticStrings.currentPlayer.l_facebook = Converter.RemoveSpecialCharacters(fb.text);
#if UNITY_EDITOR
                Debug.Log("someString  : " + Converter.RemoveSpecialCharacters(fb.text));
                Debug.Log("data saved fb : " + StaticStrings.currentPlayer.l_facebook);
#endif

            }
            else
            {
                
                StaticStrings.currentPlayer.l_facebook = "nothing";
#if UNITY_EDITOR
                Debug.LogError("empty fb ");
                Debug.Log("data saved fb : " + StaticStrings.currentPlayer.l_facebook);
   #endif
            
            }

                


        }

        public void addTwitterLink()
        {
            if (twt.text != "")
            {
               
                StaticStrings.currentPlayer.l_twitter = Converter.RemoveSpecialCharacters(twt.text);
#if UNITY_EDITOR
                Debug.Log("someString  : " + Converter.RemoveSpecialCharacters(twt.text));
                Debug.Log("data saved twt : " + StaticStrings.currentPlayer.l_twitter);
#endif

            }
            else
            { StaticStrings.currentPlayer.l_twitter = "nothing";
#if UNITY_EDITOR
                Debug.LogError("empty twitter ");
               
                Debug.Log("data saved twt : " + StaticStrings.currentPlayer.l_twitter);
#endif
                
            }


        }
        public void addLinkedinLink()
        {
            if (li.text != "")
            {
                
                StaticStrings.currentPlayer.l_linkedin = Converter.RemoveSpecialCharacters(li.text);
#if UNITY_EDITOR
                Debug.Log("someString \n : " + Converter.RemoveSpecialCharacters(li.text));
                Debug.Log("some String  : " + StaticStrings.currentPlayer.l_linkedin);
#endif

            }
            else
            {StaticStrings.currentPlayer.l_linkedin = "nothing";
#if UNITY_EDITOR
                Debug.LogError("empty linkedin ");
                
                Debug.Log("data saved linked : " + StaticStrings.currentPlayer.l_linkedin);
#endif
               
            }

        }
        public void DisableInputField(InputField inputfieldname)
        {
            inputfieldname.Select();
            inputfieldname.text = "";
            //  inputfieldname.Interactable = false ;
            inputfieldname.DeactivateInputField();
        }

  
    }

    public static class Converter
    {
        public static string RemoveSpecialCharacters(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' || c == '@')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }

}
