﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
namespace onboard
{
    public class ConventionData : MonoBehaviour
    {
        Convention[] conventions ;
        private string URL_Get_All = "http://localhost/OnboardWS/conventions.php";
        TextMeshProUGUI title;
        TextMeshProUGUI description;
        public GameObject holder_prefab;
        public GameObject holder_parent;



        void Start()
        {
            StartCoroutine(getListoFConventions());
        }
        IEnumerator getListoFConventions()
        {
        using (UnityWebRequest www = UnityWebRequest.Get(URL_Get_All))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {

#if UNITY_EDITOR
                    Debug.Log("network error ");
#endif
                }
                else
                {

                    if (www.isDone)  
                    {
                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                        if (JsonResult != "")
                        {
                            conventions = JsonHelper.getJsonArray<Convention>(JsonResult);
                            foreach (var item in conventions)
                            {
                                
                                title = holder_prefab.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>();
                                description = holder_prefab.transform.GetChild(2).gameObject.GetComponent<TextMeshProUGUI>();
                                title.text = item.title;
                                description.text = item.description;
                                GameObject p = Instantiate(holder_prefab);
                                p.transform.SetParent(holder_parent.transform);
                            }
                        }
                    }
            }
            }
           
        }
    }

}

