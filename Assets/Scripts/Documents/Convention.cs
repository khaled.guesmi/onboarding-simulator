﻿
using UnityEngine;


namespace onboard
{
    [System.Serializable]
    public class Convention  
    {
        public int id;
        public string title;
        public string description;
    }
}

