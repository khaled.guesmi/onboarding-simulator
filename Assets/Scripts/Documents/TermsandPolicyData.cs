﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class TermsandPolicyData : MonoBehaviour
    {

        bool isSaved= false;


      
    
        void Update()
        {
            if (!DataIsSaved())
            {
                SaveDataDocument();
            }
      
        }
     
 
        void SaveDataDocument()
        {
           
            if (StaticStrings.allDocuments!= null)
            {
              
                for (int i = 0; i < StaticStrings.allDocuments.docs.Count; i++)
                {
                    PlayerPrefs.SetString("doc_title_" + StaticStrings.allDocuments.docs[i].id, StaticStrings.allDocuments.docs[i].title);
                    PlayerPrefs.SetString("doc_desc_" + StaticStrings.allDocuments.docs[i].id, StaticStrings.allDocuments.docs[i].description);
                    PlayerPrefs.SetString("doc_image_" + StaticStrings.allDocuments.docs[i].id, StaticStrings.allDocuments.docs[i].image);
                    PlayerPrefs.SetString("dec_link_" + StaticStrings.allDocuments.docs[i].id, StaticStrings.allDocuments.docs[i].link);
                }
                isSaved = true;
            }
            else
            {
                isSaved = false;
#if UNITY_EDITOR
                Debug.Log("no document error instantiating docs");
#endif

            }
           
           
          

        }
   

        public bool DataIsSaved()
        {
            return isSaved;
        }

    }
}
