﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace onboard
{
    public class Level0Manager : MonoBehaviour
    {
        [Tooltip("get all the document from the server")]
        public bool getAll = false;
        [Tooltip("get one Document from the server")]
        public bool getOne = false;
        [Tooltip("document id ")]
        public int docId = 0;

        void Start()
        {
            StopAllCoroutines();
            if (getAll)
            {
                StartCoroutine(DocumentsData.Instance.GetAllDocs());
            }
            if (getOne)
            {
                StartCoroutine(DocumentsData.Instance.GetDocById(docId));
            }
            
       
        }

       
 
    }
}

