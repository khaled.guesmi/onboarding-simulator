﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Linq.Expressions;
using System;

namespace onboard
{
    public class DocumentsData : MonoBehaviour
    {
        #region properties

        private static DocumentsData _instance;
        //DocumentList document_List ; 

        private string URL_Get_All = "http://localhost/OnboardWS/GetAllDocs.php";
        private string URL_Get_ById= "http://localhost/OnboardWS/getdoc.php?id=";
        Document[] documents;
 
            #endregion


 
        #region Singletons
        public static DocumentsData Instance
        {

            get
            {

                if (_instance == null)
                {
                    _instance = FindObjectOfType<DocumentsData>();

                    if (_instance == null)
                    {
                        GameObject go = new GameObject();//create an empty gameobject 
                        go.name = typeof(DocumentsData).Name;//modify name 
                        go.AddComponent<DocumentsData>();// add this script to the new gameobject 
                        DontDestroyOnLoad(go);//dont destroy the game object on load 
                    }
                }
                return _instance;
            }
        }


        #endregion

     
        #region Get Data 



        public IEnumerator GetAllDocs()
        {
            using (UnityWebRequest www = UnityWebRequest.Get(URL_Get_All))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {

#if UNITY_EDITOR
                    Debug.Log("network Error");
#endif
                }
                else
                {

                    if (www.isDone)  
                    {
            

                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

         

                        if (JsonResult != "")
                        {
                            documents = JsonHelper.getJsonArray<Document>(JsonResult);
                            int i = 0;
                            foreach (Document d in documents)
                            { 

                                //static value
                                StaticStrings.allDocuments.docs.Add(d);

                                Debug.Log(StaticStrings.allDocuments.docs.Count);
                                //shared ad saved prefs
                                PlayerPrefs.SetString("doc_title_"+d.id, d.title);
                                PlayerPrefs.SetString("doc_desc_"+d.id, d.description);
                                PlayerPrefs.SetString("doc_image_"+d.id, d.image);
                                PlayerPrefs.SetString("dec_link_"+d.id, d.link);
                                i++;
                            }




                        }
                        else
                        {
#if UNITY_EDITOR
                            Debug.Log("********** empty response **********\n");
#endif
                        }


                    }
                }
            }
        }

        public IEnumerator GetDocById(int id)
        {
            
            URL_Get_ById += id;

            using (UnityWebRequest www = UnityWebRequest.Get(URL_Get_ById))
            {
                yield return www.SendWebRequest();

                if (!www.isNetworkError)
                {

         #if UNITY_EDITOR
                    Debug.Log(www.error + "\nNetwork connection didn't fail "+ URL_Get_ById);
        #endif

                    if (www.isDone)
                    {
                       

                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

        

                        if (JsonResult != "")
                        {
                            documents = JsonHelper.getJsonArray<Document>(JsonResult);
                            
                 
                                StaticStrings.allDocuments.docs.Add(documents.First());
#if UNITY_EDITOR
                                Debug.Log(StaticStrings.allDocuments.docs[0].ToString());
#endif
                                
                        
                        }
                    }
                    }
                
            }
        }
      
        #endregion

    


    }

}
