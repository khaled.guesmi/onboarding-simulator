﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    [System.Serializable]
    public class Document  
    {
        #region variables
        public int id;
        public string link;
        public string image;
        public string description;
        public string title;
        private static Document _inst;

      
        #endregion
        #region constructor
        public static Document Instance
        {
            get
            {
                if (_inst is null)
                {
                    _inst = new Document();
                    _inst.Init();
                }
                return _inst;
            }

        }

        private void Init()
        {
            this.id = 0;
            this.link = " ddd";
            image = "dsdfss";
            this.description = "dff";
        }
            public Document()
        {
                
        }
        
        #endregion

        public override string ToString()
        {
            return "id =" + id + " title = " + title + "description : "+description; 

        }
    }


    [System.Serializable]
    public class DocumentList
    {

        #region Properties
        public  List<Document> docs = new List<Document>();
        private static DocumentList _inst;
        #endregion


        public static DocumentList Instance
        {
            get
            {
                if (_inst is null)
                {
                    _inst = new DocumentList();
                    _inst.Init();
                }
                return _inst;
            }

        }

        private void Init()
        {
            docs = new List<Document>();
        }

        public override string ToString()
        {
            string result = "Static documents : ";
            for (int i = 0; i < docs.Count; i++)
            {
                result += docs[i].ToString() + "\n";
            }
            return result;
        }

    }
}

