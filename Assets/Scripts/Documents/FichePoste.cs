﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityScript.Steps;

namespace onboard
{
    public class FichePoste : MonoBehaviour
    {
        //get the document data 
        //id , link , image , description , title 
        //if title = job_description  affect the slot and insert data 
        //input title 
        //output data inserted from saved data
        #region properties
        [Tooltip("document name")]
        [SerializeField]
        string docTitle; //name of the document i want to assign 
        Document local_doc;
        [Tooltip("Slot UI To show ")]
        string image_link;
        public TextMeshProUGUI slot_name;
        public TextMeshProUGUI document_title;
        public TextMeshProUGUI document_subtitle;
        public TextMeshProUGUI document_main;
        public ImageShower image_shower;

        #endregion


        public Document GetWantedDocument()
        {
            local_doc = new Document();
            for (int i = 0; i < StaticStrings.allDocuments.docs.Count; i++)
            {
                if (StaticStrings.allDocuments.docs[i].title.Equals(docTitle))
                {
                    local_doc.title =  PlayerPrefs.GetString("doc_title_" + StaticStrings.allDocuments.docs[i].id);
                    local_doc.description = PlayerPrefs.GetString("doc_desc_" + StaticStrings.allDocuments.docs[i].id);
                    local_doc.image = PlayerPrefs.GetString("doc_image_" + StaticStrings.allDocuments.docs[i].id);
                    
                    local_doc.link = PlayerPrefs.GetString("dec_link_" + StaticStrings.allDocuments.docs[i].id);
                  
                    return local_doc;
                }
                
            }
            return local_doc;
        }
       public void SetSlotDocument(string docName)
        {
            if (docName == docTitle)
            {
                Document d = GetWantedDocument();
                SetDocumentSlot(d);
            }
        }
       public void SetDocumentSlot(Document d)
        {

            slot_name.text = d.title;
            document_title.text = d.title;
            document_subtitle.text = "Document :" + d.id;
            document_main.text = d.description;
            if (d.image != "")
            {
                StartCoroutine(image_shower.loadImageInUnity(d.image));
            }
        }
        private void OnEnable()
        {
            Document d = GetWantedDocument();
            SetDocumentSlot(d);
        
        }
    }
}

