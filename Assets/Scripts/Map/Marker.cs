﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace onboard
{
    [System.Serializable]
    public class Marker
    {
        public int id;
        public string mark_Lon;
        public string mark_lat;
        public string type;

        public override string ToString()
        {
            return " marker: " + id + ",ln:" + mark_Lon + ",lt:" + mark_lat + ",type: " + type;
        }

    }

}
