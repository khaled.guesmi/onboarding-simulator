﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    [System.Serializable]
    public class MAP  
    {
        public int id;
        public string longetude;
        public string latitude;
        public int zoom;
        public int mapType;
        public int width;
        public int height;
        private static MAP _inst;
        public static MAP Instance
        {
            get
            {
                if (_inst is null)
                {
                    _inst = new MAP();
                    _inst.Init();
                }
                return _inst;
            }

        }

        private void Init()
        {
           
        }
        public MAP()
        {

        }

        public override string ToString()
        {
            return "map : " + id;
        }
    }

}
