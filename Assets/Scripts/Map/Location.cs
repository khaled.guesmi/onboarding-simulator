﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


namespace onboard
{
	public class Location : MonoBehaviour
	{

		public YandexMap map;
		public static Country currentLocation;//StaticStrings.currentPlaye
		void Awake()
        {
			currentLocation = Country.Instance;
			 
		}
		void Start()
		{

			StartCoroutine("DetectCountry");
		}

		IEnumerator DetectCountry()
		{
			UnityWebRequest request = UnityWebRequest.Get("https://extreme-ip-lookup.com/json");
			request.chunkedTransfer = false;
			yield return request.Send();


			if (request.isNetworkError)
			{
#if UNITY_EDITOR
			Debug.Log("error : " + request.error);
#endif
	
			}
			else
			{
				if (request.isDone)
				{
					string s = "";
					Country res = JsonUtility.FromJson<Country>(request.downloadHandler.text);
					s += res.continent + "  ";
					s += res.city + "  ";
					s += res.country + "  ";
					s += res.lat + "  ";
					s += res.lon + " ";
					currentLocation = res;
#if UNITY_EDITOR
Debug.Log(s);
#endif
					
				}
			}
		}
 
	}

}
