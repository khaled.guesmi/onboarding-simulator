﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace onboard
	{
		  

[System.Serializable]
public class Country  
{

	public string businessName;
	public string businessWebsite;
	public string city;
	public string continent;
	public string country;
	public string countryCode;
	public string ipName;
	public string ipType;
	public string isp;
	public string lat;
	public string lon;
	public string org;
	public string query;
	public string region;
	public string status;
	private static Country _inst;

		public static Country Instance
		{
			get
			{
				if (_inst is null)
				{
					_inst = new Country();
					_inst.Init();
				}
				return _inst;
			}

		}

		private void Init()
		{

		}
	}
}