﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationDetector : MonoBehaviour
{
    IEnumerator Start()
    {
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
#if UNITY_EDITOR
            print("location is not enaled in this devices");
#endif

            yield break;

        }
      

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;

#if UNITY_EDITOR
    print("Timed  "+ maxWait);
#endif
        
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {

#if UNITY_EDITOR
    print("Timed out");
#endif
        
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
#if UNITY_EDITOR
            print("Unable to determine device location");
#endif

            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
#if UNITY_EDITOR
            print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
#endif

        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }
}