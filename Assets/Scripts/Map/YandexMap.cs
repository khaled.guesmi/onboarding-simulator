﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;
using System.Linq;

namespace onboard
{
	#region enums
	public enum colorType
	{
		blue,
		red,
		white,
		dark_orange,
		dark_blue,
		green,
		grey,
		light_blue,
		night,
		orange,
		pink,
		violet,
		yellow,
		dark_green,
		violet2,
		blue_yellow
	}
	public enum mapType
	{
		roadmap = 0,
		satellite = 1,
		geo = 2,
		terrain = 3
	}

	#endregion

	[Serializable]
	public class YandexMap : MonoBehaviour
	{
		//UI
		#region UI
		 
 
		public RawImage img; //background and map Holder
		//pannel modifier
		public TMP_Dropdown drop_map;
		public InputField Longitude_Input;
		public InputField Latitude_Input;
		//pannel Markers
		public TMP_Dropdown drop_colors;
		public InputField longitude_MInput;
		public InputField latitude_MInput;
		public GameObject loadingScreen;

		//Announcements
		public GameObject announcementPannel;
		public UnityEngine.UI.Image announcementImage;
		public TextMeshProUGUI announcementText;
		#endregion

		#region global properties
		public double longitude = 10.23989d;
		public double latitude = 36.83367d;
		public List<MarkerPoint> markers;
		[Range(0, 17)]
		//[Range(0f, 11f)]
		[Tooltip("the zoom can't be bigger than 17")]
		public int zoom = 9;
		[Range(300, 650)]
		[Tooltip("the size of the map can't be bigger than 650 on the width and 450 in Height")]
		public int mapWidth = 640;
		[Range(200, 450)]
		[Tooltip("the size of the map can't be bigger than 650 on the width and 450 in Height")]
		public int mapHeight = 400;

		public mapType mapSelected;
		public colorType selectedColor;
		public static YandexMap _instance;
		MAP[] maps;
		[HideInInspector]
		public static string url_map;
		public static List<MAP> mps ;
		[HideInInspector]
		public static List<Marker> restaurents , parkingList  , shoppingList , othersList ;
	 
		#endregion
		#region Singletons
		public static YandexMap Instance
		{ 
			get
			{

				if (_instance == null)
				{
					_instance = FindObjectOfType<YandexMap>();

					 
				}
				return _instance;
			}
		}


		#endregion
		#region local properties
		string url = "";
		string markerCoordinates = "&pt=";
		string localLon = "";
		string localLat = "";
		Country res;
		#endregion


		#region Custom region
		public string SetMarkers()
		{
			if (markers.Count > 2)
			{
				markerCoordinates += ConvertDouble(markers[0].mark_Lon) + "," + ConvertDouble(markers[0].mark_lat) + "," + ColorType(markers[0].colorSelected) + "1";
				for (int i = 1; i < markers.Count - 1; i++)
				{
					markerCoordinates += "~" + ConvertDouble(markers[i].mark_Lon) + "," + ConvertDouble(markers[i].mark_lat) + "," + ColorType(markers[i].colorSelected) + "" + i + 1;

				}
				markerCoordinates += "~" + ConvertDouble(markers[markers.Count].mark_Lon) + "," + ConvertDouble(markers[markers.Count].mark_lat) + "," + ColorType(markers[markers.Count].colorSelected) + "" + markers.Count + 1;
				return markerCoordinates;
			}
			else if (markers.Count == 2)
			{

				markerCoordinates += ConvertDouble(markers[0].mark_Lon) + "," + ConvertDouble(markers[0].mark_lat) + "," + ColorType(markers[0].colorSelected) + "1~" + ConvertDouble(markers[1].mark_Lon) + "," + ConvertDouble(markers[1].mark_lat) + "," + ColorType(markers[1].colorSelected) + "2";

				return markerCoordinates;
			}
			else if (markers.Count == 1)
			{
				markerCoordinates += ConvertDouble(markers[0].mark_Lon) + "," + ConvertDouble(markers[0].mark_lat) + "," + ColorType(markers[0].colorSelected) + "1";

				return markerCoordinates;
			}
			else
			{
				return "";
			}
		}
		public string SetMarkers2(List<Marker> markers1 , int index )
		{
			if (markers1.Count > 2)
			{
				markerCoordinates +=   markers1[0].mark_Lon  + "," +  markers1[0].mark_lat  + "," + MarkerType(index) + "1";
				for (int i = 1; i < markers.Count - 1; i++)
				{
					markerCoordinates += "~" +  markers1[i].mark_Lon + "," +  markers1[i].mark_lat  + "," + MarkerType(index) + "" + i + 1;

				}
				markerCoordinates += "~" +  markers1[markers.Count].mark_Lon  + "," +  markers1[markers.Count].mark_lat  + "," + MarkerType(index) + "";
				return markerCoordinates;
			}
			else if (markers1.Count == 2)
			{

				markerCoordinates +=  markers1[0].mark_Lon  + "," + markers1[0].mark_lat  + "," + MarkerType(index) + "1~" +  markers1[1].mark_Lon + "," +  markers1[1].mark_lat + "," + MarkerType(index) + "2";

				return markerCoordinates;
			}
			else if (markers1.Count == 1)
			{
				markerCoordinates +=  markers1[0].mark_Lon  + "," +  markers1[0].mark_lat  + "," + MarkerType(index);

				return markerCoordinates;
			}
			else
			{
				return "";
			}
		}
		void populateMapList()
		{
			string[] enumNames = Enum.GetNames(typeof(mapType));
			List<string> types = new List<string>(enumNames);
			drop_map.AddOptions(types);
		}
		void populateColorList()
		{
			string[] enumNames = Enum.GetNames(typeof(colorType));
			List<string> types = new List<string>(enumNames);

			drop_colors.AddOptions(types);
		}

		#region inside game map modif
		public void AddMarker()
		{
			MarkerPoint mp = new MarkerPoint();
			mp.mark_Lon = double.Parse(longitude_MInput.text);
			mp.mark_lat = double.Parse(latitude_MInput.text);
			mp.colorSelected = selectedColor;
			markers.Add(mp);
			AnnouncementsShow();


		}
		public void RefreshMap()
		{
			StartCoroutine(MapRefresh());//button to reload new MAP
		}
		IEnumerator Map()
		{
			/*url = "https://static-maps.yandex.ru/1.x/?lang=en-US&ll=" + ConvertDouble(longitude) + "," + ConvertDouble(latitude) +
				"&z=" + zoom + "&l=" + MapType() + "&size=" + mapWidth + "," + mapHeight + SetMarkers();*/
			url = "https://static-maps.yandex.ru/1.x/?lang=en-US&ll=" + localLon + "," + localLat +
							"&z=" + zoom + "&l=" + MapType() + "&size=" + mapWidth + "," + mapHeight + SetMarkers();

#if UNITY_EDITOR
			Debug.Log(url);
#endif
			markerCoordinates = "&pt=";


			WWW www = new WWW(url);
			yield return  
				new WaitForSeconds(3f);

			yield return www;
			img.texture = www.texture;

				yield return new WaitForSeconds(2f);
					loadingScreen.SetActive(false);

		}

		//update Map when change happen
		IEnumerator MapRefresh()
		{
			 url = "https://static-maps.yandex.ru/1.x/?lang=en-US&ll="+ ConvertDouble(longitude) + "," + ConvertDouble(latitude) +
			   "&z=" + zoom + "&l=" + MapType() + "&size=" + mapWidth + "," + mapHeight + SetMarkers(); 
			 

#if UNITY_EDITOR
			Debug.Log("refreshed map :"+url);
#endif
			markerCoordinates = "&pt=";


			WWW www = new WWW(url);
			yield return www;
			img.texture = www.texture;
			// img.SetNativeSize();
			
		}
		IEnumerator DetectCountry()
		{
			UnityWebRequest request = UnityWebRequest.Get("https://extreme-ip-lookup.com/json");
			request.chunkedTransfer = false;
			yield return request.Send();


			if (request.isNetworkError)
			{
#if UNITY_EDITOR
				Debug.Log("error : " + request.error);

#endif
			}
			else
			{
				if (request.isDone)
				{
					string s = "";
					 res = JsonUtility.FromJson<Country>(request.downloadHandler.text);
					localLat = res.lat;
					localLon = res.lon;
#if UNITY_EDITOR
					Debug.Log("this devise location :"+localLat + "," + localLon);
#endif

				}
			}
		}
		IEnumerator WaitForCoordinates()
        {
			yield return new WaitForSeconds(1f);
			StartCoroutine(Map());
		
		}
		string ConvertDouble(double d)
		{
			int i = (int)d;

			var j = (d - i) * 1000000;

			int x = (int)j;

			String s = "" + i + "." + x;
			return s;
		}
		//different map types condition
		string MapType()
		{

			if (mapSelected == mapType.roadmap)
			{
				return "map";
			}
			else if (mapSelected == mapType.satellite)
			{
				return "sat";
			}
			else if (mapSelected == mapType.geo)
			{
				return "skl";
			}
			else if (mapSelected == mapType.terrain)
			{
				return "trf";
			}
			return "map";
		}

		//different color types of the marker
		string MarkerType(int i)
        {
            if (i == 0)
            {
				return "pm2" + "rd" + "l";
			}
            else if (i==1)
            {
				return "pm2" + "do" + "l";
			}
            else if (i == 2)
            {
				return "pm2" + "db" + "l";
			}
            else
            {
				return "pm2" + "nt" + "l";
			}
        }
		string ColorType(colorType colorSelected)
		{

			if (colorSelected == colorType.blue)
			{
				return "pm2" + "bl" + "l";
			}
			if (colorSelected == colorType.dark_blue)
			{
				return "pm2" + "db" + "l";
			}
			if (colorSelected == colorType.dark_orange)
			{
				return "pm2" + "do" + "l";
			}
			if (colorSelected == colorType.green)
			{
				return "pm2" + "gnl";
			}
			if (colorSelected == colorType.grey)
			{
				return "pm2" + "gr" + "l";
			}
			if (colorSelected == colorType.light_blue)
			{
				return "pm2" + "lb" + "l";
			}
			if (colorSelected == colorType.night)
			{
				return "pm2" + "nt" + "l";
			}
			if (colorSelected == colorType.orange)
			{
				return "pm2" + "or" + "l";
			}
			if (colorSelected == colorType.pink)
			{
				return "pm2" + "pn" + "l";
			}
			if (colorSelected == colorType.red)
			{
				return "pm2" + "rd" + "l";
			}
			if (colorSelected == colorType.violet)
			{
				return "pm2" + "vv" + "l";
			}
			if (colorSelected == colorType.white)
			{
				return "pm2" + "wt" + "l";
			}
			if (colorSelected == colorType.yellow)
			{
				return "pm2" + "yw" + "l";
			}
			if (colorSelected == colorType.blue_yellow)
			{
				return "pm2" + "rd" + "l";
			}

			return "pmywl";
		}

		//if we edit the map long from the UI
		public void onEditLongitude(InputField lon)
		{
			longitude = double.Parse(lon.text);
		}

		//if we edit the map lat from the UI
		public void onEditLatitude(InputField lon)
		{
		
			latitude = double.Parse(lon.text);
		}
		//Change width and heights of the map 
		public void onWidthSliderChange(Slider s)
		{
			mapWidth = (int)s.value;

		}
		public void onHeightSliderChange(Slider s)
		{
			mapHeight = (int)s.value;

		}

		//zoom change in the map 
		public void onZoomChange(Slider s)
		{
			zoom = (int)s.value;
		}
		//different map types 
		public void onMapTypeSelected()
		{

			if (drop_map.value == 0)
			{
				mapSelected = mapType.roadmap;

			}
			if (drop_map.value == 1)
			{
				mapSelected = mapType.satellite;

			}
			if (drop_map.value == 2)
			{
				mapSelected = mapType.geo;

			}
			if (drop_map.value == 3)
			{
				mapSelected = mapType.terrain;

			}

		}

		//marker colors
		public void onMarkerColorTypeSelected()
		{
			//1 blue,red,white,	dark_orange,
			if (drop_colors.value == 0)
			{
				selectedColor = colorType.blue;
				//selectedColor

			}
			if (drop_colors.value == 1)
			{
				selectedColor = colorType.red;

			}
			if (drop_colors.value == 2)
			{
				selectedColor = colorType.white;

			}
			if (drop_colors.value == 3)
			{
				selectedColor = colorType.dark_orange;

			}
			//2  dark_blue,green,grey,light_blue,
			if (drop_colors.value == 4)
			{
				selectedColor = colorType.dark_blue;

			}
			if (drop_colors.value == 5)
			{
				selectedColor = colorType.green;

			}
			if (drop_colors.value == 6)
			{
				selectedColor = colorType.grey;

			}
			if (drop_colors.value == 7)
			{
				selectedColor = colorType.light_blue;

			}
			//3  night,orange,pink,violet,
			if (drop_colors.value == 8)
			{
				selectedColor = colorType.night;

			}
			if (drop_colors.value == 9)
			{
				selectedColor = colorType.orange;

			}
			if (drop_colors.value == 10)
			{
				selectedColor = colorType.pink;

			}
			if (drop_colors.value == 11)
			{
				selectedColor = colorType.violet;

			}
			//4 yellow,dark_green,violet2,blue_yellow
			if (drop_colors.value == 12)
			{
				selectedColor = colorType.yellow;

			}
			if (drop_colors.value == 13)
			{
				selectedColor = colorType.dark_green;

			}
			if (drop_colors.value == 14)
			{
				selectedColor = colorType.violet2;

			}
			if (drop_colors.value == 15)
			{
				selectedColor = colorType.blue_yellow;

			}

		}
        #endregion

        #region Server_Side Fill Map
		IEnumerator GenerateMapWithMarkers(string markers , MAP p)
        {
			loadingScreen.SetActive(true);
			url = "https://static-maps.yandex.ru/1.x/?lang=en-US&ll=" + p.longetude + "," + p.latitude +
						   "&z=" + p.zoom + "&l=" + TypeOfMap1(p.mapType) + "&size=" + p.width + "," + p.height + markers;

#if UNITY_EDITOR
			Debug.Log("this url from server : " + url);
#endif
			markerCoordinates = "&pt=";


			WWW www = new WWW(url);
			yield return
				new WaitForSeconds(3f);

			yield return www;
			img.texture = www.texture;

			yield return new WaitForSeconds(2f);
			loadingScreen.SetActive(false);
		}
		public void RestaurentsMap()
        {//red

			 string markersHolder = SetMarkers2(restaurents, 0);
			 
			 StartCoroutine(GenerateMapWithMarkers(markersHolder, YandexMap.mps.First()));

		}
		public void ParkingsMap()
		{//night
			string markersHolder = SetMarkers2(parkingList, 3);

			StartCoroutine(GenerateMapWithMarkers(markersHolder, YandexMap.mps.First()));
		}
		public void ShoppingsMap()
		{//dark_orange
			string markersHolder = SetMarkers2(shoppingList, 1);

			StartCoroutine(GenerateMapWithMarkers(markersHolder, YandexMap.mps.First()));
		}
		public void OthersMap()
		{//dark_green
			string markersHolder = SetMarkers2(othersList , 2);

			StartCoroutine(GenerateMapWithMarkers(markersHolder, YandexMap.mps.First()));
		}
		IEnumerator GetMapFromserver()
        {
			string url = "http://localhost/OnboardWS/map/map";
			using (UnityWebRequest www = UnityWebRequest.Get(url))
			{
				yield return www.SendWebRequest();

				if (www.isNetworkError)
				{
#if UNITY_EDITOR
					Debug.Log("no connection!!!!!");
#endif

				}
				else
				{

					if (www.isDone)
					{
					    string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
						string JsonResult2 = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data, 1, www.downloadHandler.data.Length - 2);
					 
				 if (JsonResult != "")
						{
							 
						   maps = JsonHelper.getJsonArray<MAP>(JsonResult);
							
						   mps.Add(maps.First());

                            foreach (var m in mps)
                            {
								StartCoroutine(chargeMap(m));
                            }
						} 
                       


					}
				}
			}
		}
		IEnumerator chargeMap(MAP p)
        {
			string markerCoordinates = "&pt="+p.longetude + "," + p.latitude + "," + "pm2" + "db" + "l";
			url = "https://static-maps.yandex.ru/1.x/?lang=en-US&ll=" + p.longetude + "," + p.latitude +
							"&z=" + p.zoom + "&l=" + TypeOfMap1(p.mapType) + "&size=" + p.width + "," + p.height + markerCoordinates;
			url_map = "https://static-maps.yandex.ru/1.x/?lang=en-US&ll=" + p.longetude + "," + p.latitude +"&z=" + p.zoom + "&l=" + TypeOfMap1(p.mapType) + "&size=" + p.width + "," + p.height ;

#if UNITY_EDITOR
			Debug.Log("this url from server : "+url);
#endif
			markerCoordinates = "&pt=";


			WWW www = new WWW(url);
			yield return
				new WaitForSeconds(3f);

			yield return www;
			img.texture = www.texture;

			yield return new WaitForSeconds(2f);
			loadingScreen.SetActive(false);

		}
		string TypeOfMap1(int i)
        {
			if (i == 0)
			{
				return "map";
			}
			else if (i == 1)
			{
				return "sat";
			}
			else if (i == 2)
			{
				return "skl";
			}
			else if (i == 3)
			{
				return "trf";
			}
			return "map";
		}
		IEnumerator GetAllMarkersFromServer()
        {
			string url = "http://localhost/OnboardWS/map/markers";
			using (UnityWebRequest www = UnityWebRequest.Get(url))
			{
				yield return www.SendWebRequest();

				if (www.isNetworkError)
				{
#if UNITY_EDITOR
					Debug.Log("no connection!!!!!");
#endif

				}
				else
				{

					if (www.isDone)
					{
						string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

						Debug.Log(JsonResult);
						if (JsonResult != "")
						{ 
							Marker[] markers = JsonHelper.getJsonArray<Marker>(JsonResult);
							 
							foreach (var m in markers)
							{
                                if (m.type == "restaurant")
                                {
									restaurents.Add(m);

                                }
                                else if (m.type == "parking")
								{
									parkingList.Add(m);
                                }
                                else if (m.type == "shopping")
                                {
									shoppingList.Add(m);
                                }
                                else
                                {
									othersList.Add(m);
                                }
							}
						}



					}
				}
			}
		}
		public void onZoomChangeServerMap(Slider s)
		{
			
			zoom = (int)s.value;
#if UNITY_EDITOR
			Debug.Log("zoom!!!!!"+ zoom);
#endif
			StartCoroutine("UpdateZoom", zoom);

	 
			 
		}
		IEnumerator UpdateZoom(int i)
        {
			string urlUpdate1 = "http://localhost/OnboardWS/map/UpdateMapZoom.php?zoom="+i;
			mps[0].zoom = i;

#if UNITY_EDITOR
			Debug.Log(urlUpdate1);
#endif
			using (UnityWebRequest www = UnityWebRequest.Get(urlUpdate1))
			{
				yield return www.SendWebRequest();
			
				if (www.isNetworkError)
				{
#if UNITY_EDITOR
					Debug.Log("no connection!!!!!");
#endif
					StartCoroutine("UpdateZoom", i);
				}
				else
				{

					if (www.isDone)
					{
#if UNITY_EDITOR
						Debug.Log("done loading ");
#endif
						mps[0].zoom = i;
						StartCoroutine(chargeMap(mps[0]));
					}
				}
			}
		}
		#endregion

		#endregion


		#region Announcements
		public void AnnouncementsShow()
        {
			announcementPannel.SetActive(true);
			StartCoroutine(AnimateAnnouncements());
		}
		IEnumerator AnimateAnnouncements()
        {
			 
			 
			//var tempColor = an.color;
			//Debug.Log("initial alpha " + tempColor);
			for (float i = 1; i >= 0; i -= Time.deltaTime *0.1f)
			{

				announcementImage.color = new Color(1, 1, 1, i);
				announcementText.color = new Color(1, 1, 1, i);
				yield return null;
			}
			 
			yield return new WaitForSeconds(2f);
			
			RefreshMap();
			announcementPannel.SetActive(false);
		}

		#endregion
		#region default region
		void Awake()
		{
			//img = gameObject.GetComponent<RawImage>();
			Longitude_Input.text = longitude.ToString();
			Latitude_Input.text = latitude.ToString();
			longitude_MInput.text = longitude.ToString();
			latitude_MInput.text = latitude.ToString();
			markers = new List<MarkerPoint>();
			res = new Country();
			 
			mps = new List<MAP>();
			restaurents = new List<Marker>();
			parkingList = new List<Marker>();
			shoppingList = new List<Marker>();
			othersList = new List<Marker>();
		}
		public void ReloadServerMap()
        {
			StartCoroutine(GetMapFromserver());
		}
		void Start()
		{
			loadingScreen.SetActive(true);
			
			populateMapList();
			populateColorList();
			//StartCoroutine(DetectCountry());
			//StartCoroutine(WaitForCoordinates());
			StartCoroutine(GetMapFromserver());
			StartCoroutine(GetAllMarkersFromServer());
		}
		#endregion
 
		[Serializable]
		public class MarkerPoint
		{

			public double mark_Lon;
			public double mark_lat;
			public colorType colorSelected;
			public string type;

			public MarkerPoint()
			{
				colorSelected = colorType.night;
				mark_Lon = 0.0;
				mark_lat = 0.0;

			}
		}

	}



}
