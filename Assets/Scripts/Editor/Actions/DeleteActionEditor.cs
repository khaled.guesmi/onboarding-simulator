﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace onboard
{
    [CustomEditor(typeof(DeleteActions))]
    public class DeleteActionEditor : Editor
    {
        SerializedProperty customGOList;
        SerializedProperty newActions , s_iterract;
        private void OnEnable()
        {
            customGOList = serializedObject.FindProperty("deleteActionList");
            newActions = serializedObject.FindProperty("newActions");
            s_iterract = serializedObject.FindProperty("interact");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();


            EditorGUILayout.PropertyField(s_iterract, new GUIContent("Interract"));

            if (GUILayout.Button("Add Entry to delete"))
            {
                customGOList.InsertArrayElementAtIndex(customGOList.arraySize);
            }
            if (GUILayout.Button("Add Entry to add"))
             {
                 newActions.InsertArrayElementAtIndex(newActions.arraySize);
             }

            DrawCustomObjectFieldsAdds(newActions);

            DrawCustomObjectFieldsDELETE(customGOList);

            serializedObject.ApplyModifiedProperties();
        }

        void DrawCustomObjectFieldsDELETE(SerializedProperty customList)
        {
            for (int i = 0; i < customList.arraySize; i++)
            {
               GUILayout.BeginHorizontal("box");
               GUILayout.BeginVertical(GUILayout.Width(50f));
               
               EditorGUILayout.PropertyField(customList.GetArrayElementAtIndex(i), new GUIContent("ENTRY TO DELETE "+i), GUILayout.Width(350f));
               
                if (GUILayout.Button("x", GUILayout.Width(50f)))
                {
                    customList.DeleteArrayElementAtIndex(i);
                }
                GUILayout.EndVertical();

                GUILayout.EndHorizontal();
            }
        }
        void DrawCustomObjectFieldsAdds(SerializedProperty customList)
        {
            for (int i = 0; i < customList.arraySize; i++)
            {
                GUILayout.BeginHorizontal("box");
                GUILayout.BeginVertical(GUILayout.Width(50f));

                EditorGUILayout.PropertyField(newActions.GetArrayElementAtIndex(i), new GUIContent("ENTRY TO Add "+i), GUILayout.Width(350f));

                if (GUILayout.Button("x", GUILayout.Width(50f)))
                {
                    customList.DeleteArrayElementAtIndex(i);
                }
                GUILayout.EndVertical();

                GUILayout.EndHorizontal();
            }
        }
    }
}
