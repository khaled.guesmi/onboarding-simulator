﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace onboard
{
    [CustomEditor(typeof(TriggerInteract))]
    public class TriggerInterractEditor : Editor
    {
        SerializedProperty s_selectTag, s_enterActions, s_exitActions;
        private void OnEnable()
        {

            s_selectTag = serializedObject.FindProperty("selectTag");
            s_enterActions= serializedObject.FindProperty("enterActions");
            s_exitActions = serializedObject.FindProperty("exitActions");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            s_selectTag.stringValue = EditorGUILayout.TagField("Trigger Tag:  ", s_selectTag.stringValue);

            EditorExtensions.DrawActionsArray(s_enterActions, "Trigger Enter Actions");
            EditorExtensions.DrawActionsArray(s_exitActions, "Trigger Exit Actions");
            serializedObject.ApplyModifiedProperties();//update unity UI
        }
    }

}
