﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace onboard
{

    [CustomEditor(typeof(MessageAction)), CanEditMultipleObjects]
    public class MessageActionsEditor : Editor
    {
        SerializedProperty s_messages, s_enableDialog, s_yesText, s_noText, s_yesActions, s_noActions, s_npc_name, s_npc_profile , s_tags;

        private void OnEnable()
        {
            s_messages = serializedObject.FindProperty("messages");
            s_tags = serializedObject.FindProperty("tags");
            s_enableDialog = serializedObject.FindProperty("enableDialog");
         
            s_yesText = serializedObject.FindProperty("yesText");
            s_noText = serializedObject.FindProperty("noText");
 
            s_yesActions = serializedObject.FindProperty("yesActions");
            s_noActions = serializedObject.FindProperty("noActions");
            s_npc_name = serializedObject.FindProperty("npc_name");

         s_npc_profile = serializedObject.FindProperty("m_texture");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(s_npc_name, new GUIContent("NPC NAME"));
            //show add message
            if (GUILayout.Button("Add Message"))
            {
                s_messages.InsertArrayElementAtIndex(s_messages.arraySize);
                s_tags.InsertArrayElementAtIndex(s_tags.arraySize);
            }
            
            EditorGUILayout.PropertyField(s_npc_profile, new GUIContent("NPC Image"));
            for (int i = 0; i < s_messages.arraySize; i++)
            {
                DrawMessagesEntry(s_messages.GetArrayElementAtIndex(i), "Message " + (i + 1), i);
            }
            for (int i = 0; i < s_tags.arraySize; i++)
            {
                DrawtagEntry(s_tags.GetArrayElementAtIndex(i), "Tag " + (i + 1), i);
            }

            //show enableDialog toggle, if enabled, then show the dialog properties
            GUILayout.BeginVertical("box");

            EditorGUILayout.PropertyField(s_enableDialog, new GUIContent("Enable Dialog:"));

          

            if (s_enableDialog.boolValue)
            {
                EditorGUILayout.PropertyField(s_yesText, new GUIContent("Yes Button Label"), GUILayout.Height(30f));
                EditorExtensions.DrawActionsArray(s_yesActions, "Yes Actions");

                EditorGUILayout.PropertyField(s_noText, new GUIContent("No Button Label"), GUILayout.Height(30f));
                EditorExtensions.DrawActionsArray(s_noActions, "No Actions");

               
            }
            

            GUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }

        void DrawMessagesEntry(SerializedProperty messageEntry, string label, int id)
        {
            GUILayout.BeginVertical("box");

            GUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(messageEntry, new GUIContent(label), GUILayout.Height(200f));

            if (GUILayout.Button("x", GUILayout.Width(20f)))
            {
                s_messages.DeleteArrayElementAtIndex(id);
            }

            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
        }
        void DrawtagEntry(SerializedProperty messageEntry, string label, int id)
        {
            GUILayout.BeginVertical("box");

            GUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(messageEntry, new GUIContent(label), GUILayout.Height(50f));

            if (GUILayout.Button("x", GUILayout.Width(20f)))
            {
                s_tags.DeleteArrayElementAtIndex(id);
            }

            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
        }
    }

}