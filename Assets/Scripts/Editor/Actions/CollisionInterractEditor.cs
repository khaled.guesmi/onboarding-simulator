﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace onboard
{
    [CustomEditor(typeof(CollissionInterract))]
    public class CollisionInterractEditor : Editor
    {
        SerializedProperty s_selectTag, s_enterActions, s_exitActions;
        private void OnEnable()
        {

            s_selectTag = serializedObject.FindProperty("selectTag");
            s_enterActions = serializedObject.FindProperty("enterActions");
            s_exitActions = serializedObject.FindProperty("exitActions");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            s_selectTag.stringValue = EditorGUILayout.TagField("Collision Tag:  ", s_selectTag.stringValue);

            EditorExtensions.DrawActionsArray(s_enterActions, "Collision Enter Actions");
            EditorExtensions.DrawActionsArray(s_exitActions, "Collision Exit Actions");
            serializedObject.ApplyModifiedProperties();//update unity UI
        }
    }

}