﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace onboard
{

    [CustomEditor(typeof(PlayerTriggers))]
    public class PlayerTriggerEditor : Editor
    {
        SerializedProperty s_toggleObject;
        private void OnEnable()
        {
            s_toggleObject = serializedObject.FindProperty("toggleObject");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

        

            EditorExtensions.DrawActionsArray(s_toggleObject , "Toggle Enter Actions");

            serializedObject.ApplyModifiedProperties();
        }
    
    }
}