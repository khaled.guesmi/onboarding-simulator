﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace onboard
{
    [CustomEditor(typeof(Interraction))]
    public class InteractablesEditor : Editor
    {
        SerializedProperty s_actions, s_distancePosition, s_toggleObject;
        
        //, s_spriteCursor , s_lookOnly;

        private void OnEnable()
        {
            s_actions = serializedObject.FindProperty("actions");
            s_distancePosition = serializedObject.FindProperty("distance");
            s_toggleObject = serializedObject.FindProperty("toggleObject");
          //  s_spriteCursor = serializedObject.FindProperty("spriteCursor");
          //  s_lookOnly = serializedObject.FindProperty("lookOnly");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            GUILayout.BeginVertical("box");
          //  s_spriteCursor.objectReferenceValue = EditorGUILayout.ObjectField("sprite Cursor" , s_spriteCursor.objectReferenceValue , typeof(Sprite) , false , GUILayout.Height(75f));
            
            EditorGUILayout.PropertyField(s_distancePosition, new GUIContent("Distance Position: "));
          //  EditorGUILayout.PropertyField(s_lookOnly, new GUIContent("LOOK ONLY: "));
            EditorExtensions.DrawActionsArray(s_actions, "Actions: ");
            EditorExtensions.DrawActionsArray(s_toggleObject, "parts to toggle: ");

            GUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();
        }
    }
}

