﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace onboard
{
    public static class OnboardingMenu  
    {
        [MenuItem("Khaled Tools/Create New DataManager")]
        public static void CreateNewDataManager()
        {
            GameObject currSelected = Selection.activeGameObject;
            if (currSelected)
            {
                
                
                GameObject curCOG = new GameObject("DataManager");
                curCOG.transform.SetParent(currSelected.transform);
                curCOG.AddComponent<DataManager>();

            }
            else
            {
                GameObject curCOG = new GameObject("DataManager");
                curCOG.AddComponent<DataManager>();
            }
        }

        [MenuItem("Khaled Tools/Create New ItemDebugger")]
        public static void CreateNewItemDebuger()
        {
            GameObject currSelected = Selection.activeGameObject;
            if (currSelected)
            {
                currSelected.name = "ItemDebugger";
                currSelected.AddComponent<ItemAction>();

            }
            else
            {
                GameObject curCOG = new GameObject("ItemDebugger");
                curCOG.AddComponent<ItemAction>();
            }
        }

        [MenuItem("Khaled Tools/Create New Dialog Message")]
        public static void CreateNewDiagMessage()
        {
            GameObject currSelected = Selection.activeGameObject;
            if (currSelected)
            {
                 
                currSelected.AddComponent<MessageAction>();

            }
            else
            {
                GameObject curCOG = new GameObject("NPC");
                curCOG.AddComponent<MessageAction>();
            }
        }

        [MenuItem("Khaled Tools/Create New ActivationScript")]
        public static void CreateNewActivateGameObject()
        {
            GameObject currSelected = Selection.activeGameObject;
            if (currSelected)
            {

                currSelected.AddComponent<ActivateActions>();
                
            }
            else
            {
                GameObject curCOG = new GameObject("NPC");
                curCOG.AddComponent<ActivateActions>();
            }
        }
    }

}
