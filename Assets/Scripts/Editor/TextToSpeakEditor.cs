﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace onboard
{
    [CustomEditor(typeof(TextLoader))]
    public class TextToSpeakEditor : Editor
    {

        #region properties
        SerializedProperty s_WEB_URL , s_scene_index;

        #endregion
        private void OnEnable()
        {
            s_WEB_URL = serializedObject.FindProperty("WEB_URL");
            s_scene_index = serializedObject.FindProperty("scene_index");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.LabelField("url Link:" + s_WEB_URL.stringValue);
            EditorGUILayout.PropertyField(s_scene_index);
            serializedObject.ApplyModifiedProperties();
        }

    }

}
