﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace onboard
{
    [CustomEditor(typeof(PlayerSpawnPosition))]
    public class PlayerSpawnEditor : Editor
    {

        #region properties
        SerializedProperty spawnEntries;

        #endregion

        #region Custom Methodes 
        private void OnEnable()
        {
            spawnEntries = serializedObject.FindProperty("spawnEntries");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update(); // update the value of the properties 

            //draw the spawner entry
            DrawSpawnerEntries(spawnEntries);

            serializedObject.ApplyModifiedProperties();
        }

        void DrawSpawnerEntries(SerializedProperty spawners)
        {
            GUILayout.BeginVertical("box");

            EditorGUILayout.LabelField("Spawner Entries:");

            if (spawners.arraySize == 0)
            {
                if (GUILayout.Button("Add Entries"))
                {
                    spawners.InsertArrayElementAtIndex(spawners.arraySize);
                }
            }

            for (int i = 0; i < spawners.arraySize; i++)
            {
                GUILayout.BeginHorizontal("box");
                //draw the spawner settings
                GUILayout.BeginVertical();

                EditorGUILayout.PropertyField(spawners.GetArrayElementAtIndex(i).FindPropertyRelative("prevSceneName"));
                EditorGUILayout.PropertyField(spawners.GetArrayElementAtIndex(i).FindPropertyRelative("spawnPos"));
                EditorGUILayout.PropertyField(spawners.GetArrayElementAtIndex(i).FindPropertyRelative("spawnDir"));

                GUILayout.EndVertical();

                GUILayout.BeginVertical(GUILayout.Width(20f));

                if (GUILayout.Button("X", GUILayout.Width(20f)))
                {
                    spawners.DeleteArrayElementAtIndex(i);
                }

                if (i == spawners.arraySize - 1)
                {
                    if (GUILayout.Button("+", GUILayout.Width(20f)))
                    {
                        spawners.InsertArrayElementAtIndex(spawners.arraySize);
                    }
                }

                GUILayout.EndVertical();

                GUILayout.Space(5f);

                GUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();
        }

        private void OnSceneGUI()
        {
            serializedObject.Update();

            for (int i = 0; i < spawnEntries.arraySize; i++)
            {
                spawnEntries.GetArrayElementAtIndex(i).FindPropertyRelative("spawnPos").vector3Value =
                    Handles.PositionHandle(spawnEntries.GetArrayElementAtIndex(i).FindPropertyRelative("spawnPos").vector3Value,
                    Quaternion.LookRotation(spawnEntries.GetArrayElementAtIndex(i).FindPropertyRelative("spawnDir").vector3Value));

                Handles.Label(spawnEntries.GetArrayElementAtIndex(i).FindPropertyRelative("spawnPos").vector3Value + Vector3.up * 0.5f, "Spawn from: " +
                    spawnEntries.GetArrayElementAtIndex(i).FindPropertyRelative("prevSceneName").stringValue);
            }

            serializedObject.ApplyModifiedProperties();
        }
        #endregion

    }

}
