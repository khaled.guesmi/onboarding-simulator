﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace onboard
{
 
 [CustomEditor(typeof(BaseInputs))]
public class BaseInputs_Editor : Editor
{
  
 #region Variables
        private BaseInputs targetInput;
        #endregion


        #region Bultin Methods
        void OnEnable()
        {
            targetInput = (BaseInputs)target;
        }

        public override void OnInspectorGUI(){
            base.OnInspectorGUI();

            string debugInfo = "";
            debugInfo += "walking speed x  = " + targetInput.Speedx + "\n";
            debugInfo += "walking speed y  = " + targetInput.Speedy + "\n";

             //Custom Editor Code
            GUILayout.Space(20);
            EditorGUILayout.TextArea(debugInfo, GUILayout.Height(100));
            GUILayout.Space(20);

            Repaint();
        }
        #endregion

}

}
