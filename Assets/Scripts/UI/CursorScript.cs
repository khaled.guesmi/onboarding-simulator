﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace onboard
{

    public class CursorScript : MonoBehaviour
    {
        #region properties
        [SerializeField] Transform cursor;
        [SerializeField] UnityEngine.UI.Image cursorImage;
        [SerializeField] LayerMask layerMask;

        private Camera cam;
        private RaycastHit hit;
        private Ray ray;
        private Interraction currentInteract;
        #endregion
        // Start is called before the first frame update
        void Start()
        {
            cam = Camera.main;
        }

        void Update()
        {
            cursor.position = Input.mousePosition;

            ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100f, layerMask))
            {
                cursor.gameObject.SetActive(true);

                if (hit.collider != null && currentInteract == null)
                {
                    currentInteract = hit.collider.GetComponent<Interraction>();

                    //we want to change the cursor sprite
                    //if (currentInteract.SpriteCursor == null)
                      //  cursor.gameObject.SetActive(false);

                   // cursorImage.sprite = currentInteract.SpriteCursor;
                }
            }
            else
            {
                cursor.gameObject.SetActive(false);
                currentInteract = null;
            }
        }

        void CameraSwitch(Camera cam)
        {
            this.cam = cam;
        }
    }

}