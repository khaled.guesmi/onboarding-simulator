﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{

    public class DisableObjects : MonoBehaviour
    {
        public Transform playerTransform;
        public GameObject lastObj;
        // Update is called once per frame
        void Update()
        {
            RaycastHit hit;
            Ray ray = new Ray(Camera.main.transform.position, playerTransform.position - Camera.main.transform.position);
            float distance = Vector3.Distance(playerTransform.position, Camera.main.transform.position);
            if (Physics.Raycast(ray, out hit, distance))
            {
                if (hit.collider != null)
                {
                    if (hit.collider.CompareTag(StaticStrings.building) || hit.collider.CompareTag(StaticStrings.doors))
                    {

                        if (lastObj != null)
                        {
                            if (lastObj != hit.collider.gameObject)
                            {
                                EnableObject(true);
                                lastObj = hit.collider.gameObject;
                                EnableObject(false);
                            }
                        }
                        else
                        {
                            lastObj = hit.collider.gameObject;
                            EnableObject(false);
                        }

                    }
                    else
                    {
                        if (lastObj != null)
                        {
                            EnableObject(true);
                            lastObj = null;

                        }
                    }
                }
            }
        }

        public void EnableObject(bool enableObject)
        {
            if (lastObj.transform.parent != null && (lastObj.transform.parent.CompareTag(StaticStrings.building) || lastObj.transform.parent.CompareTag(StaticStrings.doors)))
            {
                for (int i = 0; i < lastObj.transform.parent.childCount; i++)
                {
                    MeshRenderer renderer = lastObj.transform.parent.GetChild(i).GetComponent<MeshRenderer>() ?? null;
                    if (renderer != null)
                    {
                        renderer.enabled = enableObject;
                    }
                }
            }
            else if (lastObj.transform.childCount < 0)
            {
                for (int i = 0; i < lastObj.transform.parent.childCount; i++)
                {
                    MeshRenderer renderer = lastObj.transform.parent.GetChild(i).GetComponent<MeshRenderer>() ?? null;
                    if (renderer != null)
                    {
                        renderer.enabled = enableObject;
                    }
                }
            }
            else
            {
                MeshRenderer renderer = lastObj.GetComponent<MeshRenderer>() ?? null;
                renderer.enabled = enableObject;
            }

        }
    }

}