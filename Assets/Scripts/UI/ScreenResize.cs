﻿using UnityEngine;


namespace onboard
{
    public class ScreenResize : MonoBehaviour
    {
        public RectTransform panelRectTransform; //get panelRect to resize it
                                                 // Start is called before the first frame update
        void Start()
        {
            Screen.SetResolution(1080, 1920, true);
        }

        // Update is called once per frame
        void Update()
        {
            SetScreenSize();
        }



        #region custom mehodes
        void SetScreenSize()
        {
            Screen.SetResolution(1080, 1920, true);
            panelRectTransform.anchorMin = new Vector2(1, 0);
            panelRectTransform.anchorMax = new Vector2(0, 1);
            panelRectTransform.pivot = new Vector2(0.5f, 0.5f);
        }
        #endregion

    }


}
