﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace onboard
{
    public class DragWindow : MonoBehaviour, IDragHandler ,IPointerDownHandler
    {

        #region properties
        [SerializeField]
        RectTransform dragRectTransform;
        [SerializeField]
        Canvas canvas;
 
        #endregion


        private void Awake()
        {

            if (dragRectTransform == null)
            {
                dragRectTransform = transform.parent.GetComponent<RectTransform>();
            }
            if (canvas == null)
            {
                Transform targetTransform = transform.parent;

                while (targetTransform != null)
                {
                    canvas = targetTransform.GetComponent<Canvas>();

                    if (canvas != null )
                    {
                        break;
                    }
                    targetTransform = targetTransform.parent;
                }

            }
        }
    
        public void OnDrag(PointerEventData eventData)
        {
            dragRectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        }

       
        public void OnPointerDown(PointerEventData eventData)
        {
            dragRectTransform.SetAsLastSibling();
        }
    }

}
