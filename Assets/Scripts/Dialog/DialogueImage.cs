﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace onboard
{
    public class DialogueImage : Actions
    {
        //params from unity 
        //research tool 
        public int npc_number = 0;
        [Tooltip("scene dialogue that i want to insert")]
        public int sceneIdex = 0;
        [HideInInspector]
        public string npc_name;
        [HideInInspector]
        [Tooltip("the image from Server")]
        Texture m_Texture;
        //params server 
        // url to search for data 
        //return list of messages
        [HideInInspector]
        public List<Speech> speaches = new List<Speech>();
        public List<Image> images = new List<Image>();
        [HideInInspector]
        public List<string> messages = new List<string>();
        [HideInInspector]
        public List<string> Messages { get { return messages; } }


        public override void Act()
        {
            //get the message 
            //affect message
            //get the image class 
            //get the image lik
            //affect image link to a texture 
            //set print message request

            instatiateDialogue(npc_number);
        }


        public IEnumerator loadImageInUnityByNPCName(string npc_name)
        {
            string url = "http://localhost/OnboardWS/imagebyNPC.php?npc_name="+ npc_name;
           
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
#if UNITY_EDITOR
      Debug.Log(www.error);
#endif
          
            }
            else
            {
                if (www.isDone) //test if he connection is reached 
                {
                    string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
#if UNITY_EDITOR
 Debug.Log("image : " + JsonResult);
#endif
                   
                    if (JsonResult != "")
                    {
                        Image[] entities = JsonHelper.getJsonArray<Image>(JsonResult);

                        foreach (Image p in entities)
                        {

                            images.Add(p);
                            StartCoroutine(ImageShower.Instance.loadImageInUnity(p.link));//3
                            StartCoroutine(loadImageInUnityTexture(p.link));//4
                        }
                    }
                    if (JsonResult == "")
                    {

                    }
                }

                }

        }

        public IEnumerator loadImageInUnityTexture(string url)
        {

            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                if (myTexture != null)
                {
                    m_Texture = myTexture;
                }


            }

        }

        public void instatiateDialogue(int k)
        {
            Speech s = TextLoader.speaches[k];
            Debug.Log(s.ToString());
            bool alreadyExist = speaches.Contains(s);
            if (!alreadyExist)
            {
                speaches.Add(s);
            }
            foreach (var sp in speaches)
            {
                messages.Add(sp.text);
                Debug.Log("speech : " + sp.text + "\n messages.Count : "+ messages.Count);
                
            }
            

             StartCoroutine(loadImageInUnityByNPCName(s.npc_name));//2

            DialogueSystem.Instance.PrintMessages(messages, false, null, null, "yes", "no", s.npc_name, m_Texture);
        }
     
        // show and apply message

        IEnumerator WaitForServerRESP()
        {

            yield return new WaitForSeconds(2f);
        }
    }

}
