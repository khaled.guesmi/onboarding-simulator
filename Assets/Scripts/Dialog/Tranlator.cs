﻿using onboard;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace oboard
{
    public class Tranlator : MonoBehaviour
    {
        [Tooltip("enter one of the keys that you specify in your (txt) file for all languages.\n\n# for example: [HOME=home]\n# the key here is [HOME]")]
        [Header("Enter your word key here.")]
        [SerializeField]
        string[] keys ;
        List<string> translated_messages = new List<string>();
        public MessageAction messages;
        //public TMP_Text holder;
        // Start is called before the first frame update
        private void Start()
        {
            Traduction();
        }
        public void Traduction()
        {

            translated_messages = messages.Messages;
            for (int i = 0; i < translated_messages.Count; i++)
            {
                translated_messages[i]=GameMultiLang.GetTraduction(keys[i]);
#if UNITY_EDITOR
   Debug.Log(translated_messages[i]);
#endif
             
            }
             
        }
    }

}
