﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement ;


namespace onboard
{
	public class DialogueTrigger : MonoBehaviour
	{
		public Dialogue dialogue;
		public DialogueManager dialogueManager;
		bool showOnce = false;
 
 
		private void Start()
		{ 
			TriggerDialogue();
			if (StaticStrings.nameNPC == "")
			{
				StaticStrings.nameNPC = "no one";
			}
		}
		void Update()
		{
			if (!showOnce)
			{
				if (StaticStrings.currentDialogue != null )
				{
		#if UNITY_EDITOR
			Debug.Log("this is dialog sentenses ? : " + dialogue.sentences.Count);
		#endif
                   
					if (StaticStrings.nameNPC == "")
					{
						StaticStrings.nameNPC = "no one";
					}
					
					showOnce = true;
				}
				else
				{
#if UNITY_EDITOR
					Debug.LogError("Diag Trig something wrong");
#endif

					showOnce = true;
				}
				
			}

	 
		}

		public void TriggerDialogue()
		{ 
				try
				{
					dialogueManager.StartDialogue(StaticStrings.currentDialogue);
				}
				catch (System.Exception e)
				{
#if UNITY_EDITOR
				Debug.LogError("Diag Trig something wrong   "+ e.Message);
#endif
			 
					throw;
				}
				 
			
			
		}
	}

}
