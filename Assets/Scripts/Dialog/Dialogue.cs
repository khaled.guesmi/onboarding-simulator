﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    [System.Serializable]
    public class Dialogue
    {

        public string name;

        [TextArea(3, 10)]
        public List<string> sentences;

        private static Dialogue _inst;
        public static Dialogue Instance
        {
            get
            {
                if (_inst is null)
                {
                    _inst = new Dialogue();
                    _inst.Init();
                }
                return _inst;
            }

        }

        private void Init()
        {

        }

    }

}
