﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro ;
using UnityEngine.SceneManagement;

namespace onboard
{
	    [System.Serializable]
	public class DialogueManager : MonoBehaviour
	{

		#region properties
		[Header("UI Dialogue Box")]
		public TMP_Text nameText;
		public TMP_Text dialogueText;
 
		private Queue<string> sentences;
	 
	    SceneLoader sceneLoader;

		#endregion
		void Awake()
        {
			sentences = new Queue<string>();
			sceneLoader = GetComponent<SceneLoader>();
		 
		}

	 


		public void StartDialogue(Dialogue dialogue)
		{
			 
				nameText.text = dialogue.name;

				sentences.Clear();
			dialogue = StaticStrings.currentDialogue;
			 

		 
			string text = "";
				foreach (string sentence in dialogue.sentences.ToArray())
				{

					sentences.Enqueue(sentence);
					text += sentence + " .\n ";

				}


		 

				DisplayNextSentence();
	 
		}

		public void DisplayNextSentence()
		{
			//test how many are left 

			if (sentences.Count == 0)
			{
				sentences.Clear();

				if (StaticStrings.sceneIndex == 0)
				{
					if (StaticStrings.nameNPC == "Aroua Hdiji")
					{
						 
						
						EndDialogue();
						return;
					}
				}
				EndDialogue();
				return;

			}

			string sentence = sentences.Dequeue();

			StopAllCoroutines();
			StartCoroutine(TypeSentence(sentence));
		}

		IEnumerator TypeSentence(string sentence)
		{
			dialogueText.text = "";
			foreach (char letter in sentence.ToCharArray())
			{
				dialogueText.text += letter;
				yield return null;
			}
		}

		void EndDialogue()
		{
			 
			Debug.Log(("end diag"));
			if (StaticStrings.sceneIndex == 0)
			{
				sceneLoader.LoadNextScene();
			}

		}
		void SpeakTheText(string sentence)
		{
			StopAllCoroutines();
 
		}


	}

}

