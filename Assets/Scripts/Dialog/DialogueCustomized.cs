﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Linq;

namespace onboard
{
    public class DialogueCustomized : Actions
    {
        //params from unity 
        //research tool 
        [Tooltip("scene dialogue that i want to insert")]
        public int sceneIdex = 0;
        [Tooltip("the name of the npc to say the dialogue")]
        public string npc_name;
        [SerializeField]
        [Tooltip("the image from Server")]
        Texture m_Texture;
        //params server 
        // url to search for data 
        //return list of messages 
        public List<Speech> speaches = new List<Speech>();
        [SerializeField]
        List<string> messages;
        [HideInInspector]
        public List<string> Messages { get { return messages; } }
        //url
     
        string WEB_URL = "http://localhost/OnboardWS/DiagByScene.php?scene_index=";
        string url_name = "&npc_name=";
        void Awake()
        {
            messages = new List<string>();
        }
        public override void Act()
        {

            WEB_URL += sceneIdex + url_name + npc_name;
            
            StartCoroutine(GetDiag(WEB_URL));

             StartCoroutine(WaitForServerRESP());
          
            //DialogueSystem.Instance.PrintMessages(messages , false, null, null, "yes", "no", "Manager");

        }



        public IEnumerator GetDiag(string url)
        {
         
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();
                string error = www.error;
                if (error != null)
                {
#if UNITY_EDITOR
                    Debug.LogError(error);
#endif
                }
                else
                {
                    if (www.isDone) //test if he connection is reached 
                    {

                        string JsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);


                        if (JsonResult != "")
                        {
                            Speech[] entities = JsonHelper.getJsonArray<Speech>(JsonResult);
                          
                            foreach (var d in entities)
                            {
                                messages.Add(d.text);
                            }
                            DialogueSystem.Instance.PrintMessages(messages, false, null, null, "yes", "no", "Manager", m_Texture);
                        }
                        if (JsonResult == "")
                        {

                        }


                    }
                }
            }
        }

        // show and apply message

        IEnumerator WaitForServerRESP()
        {

            yield return new WaitForSeconds(2f);
        }
    }

}
