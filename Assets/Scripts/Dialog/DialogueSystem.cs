﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System.Resources;

namespace onboard
{

	[RequireComponent(typeof(TextToSpeech))]
	public class DialogueSystem : MonoBehaviour
	{
		#region properties
		public static DialogueSystem Instance { get; private set; }
		[SerializeField]
		TextMeshProUGUI messageText;
		[SerializeField]
		TextMeshProUGUI yesText;
		[SerializeField]
		TextMeshProUGUI noText;
		LevelManager lm;
		[SerializeField]
		GameObject panel; //our pannel of ui dialog
		[SerializeField]
		Button yesButton; //button for yes actions 
		[SerializeField]
		Button noButton; // button for no actions 
		[SerializeField]
		TextMeshProUGUI nameNPC; // name of the npc
		[SerializeField]
		UnityEngine.UI.RawImage npc_profile;

		TextToSpeech textToSpeech;
		List<string> currentMessages = new List<string>();
		int msgId = 0;

		#endregion

		#region default methodes 
		void Awake()
		{
			Instance = this;
			textToSpeech = GetComponent<TextToSpeech>();
		}

		void Start()
		{
            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
				panel.SetActive(true);
				lm = GameObject.Find("LevelManagerCanvas").GetComponent<LevelManager>();
			}
            else
            {
				panel.SetActive(false);
			}
			
		}
		#endregion

		#region custom meths 
		public void HideDialog()
		{
			panel.SetActive(false);
			messageText.text = "";
			nameNPC.text = "";
        
		    
			StopAllCoroutines();
			msgId = 0;
		}

		//print message in the ui text and show it to the player with all the dialogue actions availbe 
		public void PrintMessages(List<string> messages, bool dialog, List<Actions> yesActions = null, List<Actions> noActions = null, string yes = "yes", string no = "no" , string npc_name="npc")
		{
			nameNPC.text = npc_name;
			msgId = 0;
			yesButton.transform.parent.gameObject.SetActive(false);
			string m = "";

			currentMessages = messages;

			panel.SetActive(true);

			for (int i = 0; i < messages.Count; i++)
			{
				m += messages[i] + "\n";

			}
			//before corroutine test if there is a dialog so we can assign actions to buttons
			if (dialog)
			{
				//assign actions to buttons
				//yes actions 
				yesText.text = yes;
				yesButton.onClick.RemoveAllListeners();
				yesButton.onClick.AddListener(delegate
				{
					HideDialog();

					if (yesActions != null)
						AssignActionsToButtons(yesActions);
					else
						yesButton.gameObject.SetActive(false);
				});
				//no actions 
				noText.text = no;
				noButton.onClick.RemoveAllListeners();
				noButton.onClick.AddListener(delegate
				{
					HideDialog();

					if (noActions != null)
						AssignActionsToButtons(noActions);
					else
						noButton.gameObject.SetActive(false);

				});
			}
		 
			StartCoroutine(ShowMultipleMessages(dialog));
		}
		public void PrintMessages(List<string> messages, List<Actions> chainActions = null)
		{
			Debug.Log("Show messages 2 argument");
			msgId = 0;
			yesButton.transform.parent.gameObject.SetActive(false);
			currentMessages = messages;
			panel.SetActive(true);
			StartCoroutine(ShowMultipleMessages(false, chainActions));
		}
		public void PrintMessages(List<string> messages, bool dialog, List<Actions> yesActions = null, List<Actions> noActions = null, string yes = "yes", string no = "no", string npc_name = "npc" , Texture npc_prof = null)
		{
			if (npc_prof != null)
			{
				npc_profile.texture = npc_prof;
			}
			nameNPC.text = npc_name;
			msgId = 0;
			yesButton.transform.parent.gameObject.SetActive(false);
			string m = "";

			currentMessages = messages;

			panel.SetActive(true);

			for (int i = 0; i < messages.Count; i++)
			{
				m += messages[i] + "\n";

			}
			//before corroutine test if there is a dialog so we can assign actions to buttons
			if (dialog)
			{
				//assign actions to buttons
				//yes actions 
				yesText.text = yes;
				yesButton.onClick.RemoveAllListeners();
				yesButton.onClick.AddListener(delegate
				{
					HideDialog();

					if (yesActions != null)
						AssignActionsToButtons(yesActions);
					else
						yesButton.gameObject.SetActive(false);
				});
				//no actions 
				noText.text = no;
				noButton.onClick.RemoveAllListeners();
				noButton.onClick.AddListener(delegate
				{
					HideDialog();

					if (noActions != null)
						AssignActionsToButtons(noActions);
					else
						noButton.gameObject.SetActive(false);

				});
			}
			//show multiple messages 
			StartCoroutine(ShowMultipleMessages(dialog));
		}

		/*public IEnumerator ShowMultipleMessages(List<string> messages , bool useDialog)
		{
            if (SceneManager.GetActiveScene().buildIndex == 1)
			{
				if (messages != null)
				{

				
				 
					StartCoroutine(TypeSentence(messages[msgId]));
			 
					while (msgId < messages.Count)
					{
						if (Input.GetKeyDown(KeyCode.Space) || (Input.GetMouseButtonDown(0)) && Extensions.IsMouseOverUI())
						{
							//next message or exit conversation
							msgId++;
							if (msgId < messages.Count)
							{
								
								StartCoroutine(TypeSentence(messages[msgId]));
								//textToSpeech.TextToSpeechAudio(messages[msgId]);
								yield return new WaitForSeconds(2f);
							 
							}

						}
						yield return null;
					}

					if (useDialog)
					{
						yesButton.transform.parent.gameObject.SetActive(true);

					}
					else
					{
						panel.SetActive(false);
					 
						lm.SceneLoad("Scene0");
					}


				}
            }
            else
            {
				if (messages != null)
				{
					 
					StartCoroutine(TypeSentence(messages[msgId]));
					while (msgId < messages.Count)
					{
						if (Input.GetKeyDown(KeyCode.Space) || (Input.GetMouseButtonDown(0)) && Extensions.IsMouseOverUI())
						{
							//next message or exit conversation
							msgId++;
							if (msgId < messages.Count)
							{
								 
								StartCoroutine(TypeSentence(messages[msgId]));
							}

						}
						yield return null;
					}

					if (useDialog)
					{
						yesButton.transform.parent.gameObject.SetActive(true);

					}
					else
					{
						panel.SetActive(false);
					}


				}
			}
			


		}*/
		public IEnumerator ShowMultipleMessages(bool useDialog, List<Actions> chainActions = null)
		{
			if (SceneManager.GetActiveScene().buildIndex == 1)
			{
				messageText.text = currentMessages[msgId];

				while (msgId < currentMessages.Count)
				{
					if (Input.GetKeyDown(KeyCode.Space) || (Input.GetMouseButtonDown(0) && Extensions.IsMouseOverUI()))
					{
						msgId++;

						if (msgId < currentMessages.Count)
							messageText.text = currentMessages[msgId];

						if (!useDialog && msgId == currentMessages.Count)
						{
							if (chainActions != null)
								Extensions.RunActions(chainActions.ToArray());
						}
					}

					if (useDialog && msgId == currentMessages.Count - 1)
						yesButton.transform.parent.gameObject.SetActive(true);

					yield return null;
				}

				if (!useDialog)
                {
					HideDialog();
					lm.SceneLoad("QA_Office_Level_01_Hall");
				}

            }
            else
            {
				messageText.text = currentMessages[msgId];

				while (msgId < currentMessages.Count)
				{
					if (Input.GetKeyDown(KeyCode.Space) || (Input.GetMouseButtonDown(0) && Extensions.IsMouseOverUI()))
					{
						msgId++;

						if (msgId < currentMessages.Count)
							messageText.text = currentMessages[msgId];

						if (!useDialog && msgId == currentMessages.Count)
						{
							if (chainActions != null)
								Extensions.RunActions(chainActions.ToArray());
						}
					}

					if (useDialog && msgId == currentMessages.Count - 1)
						yesButton.transform.parent.gameObject.SetActive(true);

					yield return null;
				}

				if (!useDialog)
				{
					panel.SetActive(false); 
					HideDialog();

				}
			}
			
 

		}


		public void AssignActionsToButtons(List<Actions> actions = null)
		{
			List<Actions> localActions = actions;
			for (int i = 0; i < localActions.Count; i++)
			{
				localActions[i].Act();
			}
		}



		IEnumerator TypeSentence(string sentence)
		{
			messageText.text = "";
			char[] sentenceC = sentence.ToCharArray();
			foreach (char letter in sentenceC)
			{
				messageText.text += letter;
				yield return  null;
				if (Input.GetKeyDown(KeyCode.Space) || (Input.GetMouseButtonDown(0)) && Extensions.IsMouseOverUI())
                    if (!letter.Equals(sentenceC[sentence.ToCharArray().Length + 1]))
                    {
						messageText.text = "";

					}
			}
			 
			yield return new WaitForSeconds(2f);
		}




		#endregion
	}

}
