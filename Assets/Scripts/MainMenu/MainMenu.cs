﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace onboard
{
    public class MainMenu : MonoBehaviour
    {

        #region properties
        [Header("Menu UI reference")]
        [Tooltip("assign setting menu  pannel here")]
        public GameObject options;   //UI Setting game object
        [Tooltip("assign main menu pannel here")]
        public GameObject menu;      //UI menu game object

        #endregion
 

        #region Methodes
        //load settings menu
        public void LoadOptionsMenu()
        {
            options.SetActive(true); //activate this game object
            menu.SetActive(false);   //deactivate this game object
        }
        //show main menu 
        public void LoadMenu()
        {
            options.SetActive(false);
            menu.SetActive(true);
        }

        //exit game function
        public void ExitGame()
        {
     
            Application.Quit(); //quit the game window on build
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false; //stop editor play mode on Editor
#endif
        }




        #endregion

    }

}
