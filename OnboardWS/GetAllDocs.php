<?php
require "connDB.php";


$sql = 'SELECT * FROM documents WHERE 1'  ;

$result = $conn->query($sql);

$response = array(); 

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
    $response []= array("id"=>$row["id"],
    "link"=>$row["link"],
    "image"=>$row["image"],
    "description"=>$row["description"],
    "title"=>$row["title"]);
    }
} 
$conn->close();

echo json_encode($response);

?>