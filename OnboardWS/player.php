<?php

require "connDB.php";

$sql = "SELECT * FROM Player WHERE 1" ;

$result = $conn->query($sql);


$response = array(); 


if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
    $response []= array("id"=>$row["id"],
    "name"=>$row["nom"],
    "family_name"=>$row["prenom"],
    "badge_id"=>$row["badge_id"],
    "poste"=>$row["poste"], 
    "tel"=>$row["tel"],
    "email"=>$row["email"],
    "l_facebook"=>$row["l_facebook"],
    "l_twitter"=>$row["l_twitter"],
    "l_linkedin"=>$row["l_linkedin"],
    "hobbies"=>$row["hobbies"],
    "citation"=>$row["citation"],
    "accepted_turms"=>$row["accepted_turms"]);

    }
} else {
    echo  " \nfetch unsuccessefull or list empty ";
}
$conn->close();
echo json_encode($response);


?>
