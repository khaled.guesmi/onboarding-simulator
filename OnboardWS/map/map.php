<?php 

require "../connDB.php";

$sql = 'SELECT  * FROM map LIMIT 1'  ;

$result = $conn->query($sql);
$response = array(); 

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
    $response []= array("id"=>$row["id"],
    "longetude"=>$row["longetude"],
    "latitude"=>$row["latitude"],
    "zoom"=>$row["zoom"],
    "mapType"=>$row["mapType"],
    "width"=>$row["width"],
    "height"=>$row["height"]);
    }
} 
$conn->close();

echo json_encode($response);
?>
